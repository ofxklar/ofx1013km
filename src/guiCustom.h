#ifndef GUICUSTOM_H
#define GUICUSTOM_H

//void testApp::drawDelayMonitor(int x, int y, float ratio);
void drawFadder(int x, int y, int w, int val);
void drawFadderVert(int x, int y, int h, int val);
void drawSwitch(int x, int y, bool mode);

#endif
