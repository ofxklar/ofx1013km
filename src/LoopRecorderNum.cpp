#include "LoopRecorderNum.h"

LoopRecorderNum::LoopRecorderNum()
{
    i_current_frame = 0;

    i_frame_start = 0;
    i_frame_stop = 0;

    loop_lenght = 0;

    record_mode = false;
    play_mode = false;
}

void LoopRecorderNum::setBufferLenght(int buffer_lenght_in){
    buffer_lenght = buffer_lenght_in;
    cout << "buffer lenght " << buffer_lenght << endl; 
}

void LoopRecorderNum::setLoopStart(int i_start)
{
    record_mode = true;
    i_frame_start = i_start;
    cout << "buffer lenght 2 " <<buffer_lenght << endl;
}

void LoopRecorderNum::setLoopStop(int i_stop)
{
  record_mode = false;
  i_frame_stop = i_stop;
  if (i_stop<i_frame_start){
    i_frame_stop = i_stop;
    loop_lenght = i_frame_stop + buffer_lenght - i_frame_start;
  }else{
    loop_lenght = i_frame_stop - i_frame_start;
  }
}

void LoopRecorderNum::setLoop(int i_frame)
{

    cout << "buffer lenght 2 " <<buffer_lenght << endl;
  if (record_mode){
    record_mode = false;
    i_frame_stop = i_frame;
    loop_lenght = i_frame_stop - i_frame_start;
  } else {
    record_mode = true;
    i_frame_start = i_frame;
    i_current_frame = i_frame;
  }
}

int LoopRecorderNum::getRecordMode()
{
  return record_mode;
}

void LoopRecorderNum::idle()
{
  if (play_mode){
    i_current_frame ++;
    if (i_current_frame >= buffer_lenght){
        i_current_frame = 0;
    }
    
    if (i_current_frame == i_frame_stop){
      i_current_frame = i_frame_start;
    }
  }
}

void LoopRecorderNum::play()
{
  play_mode = true;
}

void LoopRecorderNum::stop()
{
  play_mode = false;
}

void LoopRecorderNum::setPlayMode(){
  play_mode = !play_mode;
}

int LoopRecorderNum::getPlayMode(){
  return play_mode;
}

int LoopRecorderNum::getCurrentFrameNum(){
  return i_current_frame;
}

int LoopRecorderNum::getFrameStart()
{
  return i_frame_start;
}

int LoopRecorderNum::getFrameStop()
{
  return i_frame_stop;
}


LoopRecorderNum::~LoopRecorderNum()
{
  //dtor
}
