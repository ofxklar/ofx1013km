#ifndef SCREENFRAME_H
#define SCREENFRAME_H

#define N_FRAME_MAX 20000

#include "utils.h"

class ScreenFrame
{
 public:
  ScreenFrame();
  virtual ~ScreenFrame();
  void setImage(ofxCvGrayscaleImage & image, ofBlendMode blend_mode);
  void setImage(ofImage & image, ofBlendMode blend_mode);

  void setAnchorPercent(int x_anchor, int y_anchor);
  void setPos(ofPoint pos);
  void setPos(int x, int y);
  void setBlendMode(int blend_mode);
  void setSize(int w, int h);
  void setRatio(float ratio);
  void setAngle(int angle);
  void setWarpOut(ofPoint point_in[4]);
  void initWarp();

  ofPoint getPos();
  float getRatio();
  int getAngle();
  ofPoint getWarpPoint(int i);

  void update(ofxCvGrayscaleImage & image);

  void drawFrame();
  void drawImage();  

  void changeReference(int & x, int & y);

  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);

 protected:


 private:
  int x_bak;
  int y_bak;
  int angle_bak;

  int pos_x;
  int pos_y;
  int angle;
  float ratio_x;
  float ratio_y;
  draggableVertex perspectiveVertices[4];
  

  int width;
  int height;
  ofPoint center;
  ofPoint anchorPercent;
  ofPoint point0;
  ofPoint point1;


  ofBlendMode blend_mode;


  draggableVertex centerPoint;
  ofPoint warp_in[4];
  ofPoint warp_out[4];
  ofxCvGrayscaleImage image_out;

};

#endif // SCREENFRAME_H
