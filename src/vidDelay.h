#ifndef VIDDELAY_H
#define VIDDELAY_H

#include "ofMain.h"
#include "LoopRecorder.h"
#include "utils.h"

#define DELAYMAX 40

class VidDelay
{
 public:
  VidDelay();
  virtual ~VidDelay();
  
  void init(LoopRecorder & recorder);
  void update(bool impulse);
  void drawMonitor(int x, int y, int width);
  void setDelayLenght(int x);

 protected: 
 private:

  LoopRecorder recorder;
  int     width;
  int     height;
  int     pixel_type;
  int     buffer_lenght;
  int     i_delay[DELAYMAX];
  int     n_delay;
  int     delay_lenght;
  bool    mode_delay_fixe;
  bool    mode_delay_gray;
  bool    mode_delay_end;
  bool    mode_delay_impulse;
  int     delay_decrease_value;
  int     delay_tap;
  int     delay_tap_n;
  int     delay_learn_tap;
  int     delay_offset;
  bool    mode_delay_nano;
  ofImage sortie;
  bool mode_live;
};

#endif
