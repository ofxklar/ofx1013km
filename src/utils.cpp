#include "utils.h"

void hsv2rgb(int h, int & r, int & g, int & b){
  int H = h/60;
  float hh = h/60.-H;

  int resul[3];
  switch(H){
  case 0:
    r = 255;
    g = hh*255;
    b = 0;
    break;
  case 1:
    r = 255-255*hh;
    g = 255;
    b = 0;
    break;
  case 2:
    r = 0;
    g = 255;
    b = hh*255;
    break;
  case 3:
    r = 0;
    g = 255-hh*255;
    b = 255;
    break;
  case 4:
    r = hh*255;
    g = 0;
    b = 255;
    break;
  case 5:
    r = 255;
    g = 0;
    b = 255-hh*255;
    break;
  }

  r = max(0, min(r, 255));
  g = max(0, min(g, 255));
  b = max(0, min(b, 255));

}


void showThreshold(ofImage & masque,
		   ofxCvGrayscaleImage & gray_image, 
		   ofTexture & rgba_image){
  
}

void setMasque(ofImage & masque, 
	       ofxCvGrayscaleImage & gray_image, 
	       ofTexture & rgba_image){
  int w = gray_image.width;
  int h = gray_image.height;

  //cout << "h " << gray_image.height << " w " << gray_image.width << endl
  //   << "h_s";
  unsigned char *pixels_masque = masque.getPixels();
  unsigned char *pixels_image = gray_image.getPixels();
  unsigned char *pixels2;
  pixels2 = new unsigned char [h*w*4];
  for (int i = 0; i < h*w; i++){
      pixels2[i*4] = pixels_image[i];
      pixels2[i*4+1] = pixels_image[i];
      pixels2[i*4+2] = pixels_image[i];
      pixels2[i*4+3] = pixels_masque[i*4+3];
  }


  rgba_image.loadData(pixels2, w, h, GL_RGBA);
  free(pixels2);
}
void cvBlackToAlpha(ofxCvGrayscaleImage & gray_image, ofTexture & rgba_image){
  int gray_h = gray_image.getHeight();
  int rgba_h = rgba_image.getHeight();  
  int gray_w = gray_image.getWidth();  
  int rgba_w = rgba_image.getWidth();
  unsigned char *pixels2;
  
  if (gray_h*gray_w  != rgba_h*rgba_w){
        cout << "cvGrayToRgba: les images ne font pas la meme tailles";
  } 

  unsigned char* pixels1 = gray_image.getPixels();
  pixels2 = new unsigned char [rgba_h*rgba_w*4];

  for (int i = 0; i < gray_h*gray_w; i++){
    pixels2[i*4] = 255;
    pixels2[i*4+1] = 255;
    pixels2[i*4+2] = 255; 
    pixels2[i*4+3] = pixels1[i];
  } 

  rgba_image.loadData(pixels2, rgba_w, rgba_h, GL_RGBA);
  free(pixels2);
}

void cvGrayToRgba(ofxCvGrayscaleImage & gray_image, ofTexture & rgba_image, int r, int g, int b, bool noir){
  int gray_h = gray_image.getHeight();
  int rgba_h = rgba_image.getHeight();  
  int gray_w = gray_image.getWidth();  
  int rgba_w = rgba_image.getWidth();
  unsigned char *pixels2;
  
  if (gray_h*gray_w  != rgba_h*rgba_w){
        cout << "cvGrayToRgba: les images ne font pas la meme tailles";
  } 
  
  unsigned char* pixels1 = gray_image.getPixels();
  pixels2 = new unsigned char [rgba_h*rgba_w*4];

  if (noir){
    for (int i = 0; i < gray_h*gray_w; i++){
      pixels2[i*4] = r*pixels1[i];
      pixels2[i*4+1] = g*pixels1[i];
      pixels2[i*4+2] = b*pixels1[i];
      
      if (pixels1[i] > 20) {
	pixels2[i*4+3] = 255;
      } else {
	pixels2[i*4+3] = 0;
      }
    }
  }else{
      
    for (int i = 0; i < gray_h*gray_w; i++){
      pixels2[i*4] = r*pixels1[i];
      pixels2[i*4+1] = g*pixels1[i];
      pixels2[i*4+2] = b*pixels1[i];
      
      if (pixels1[i] < 220) {
	pixels2[i*4+3] = 255;
      } else {
	pixels2[i*4+3] = 0;
      }
    }
  }
  
  rgba_image.loadData(pixels2, rgba_w, rgba_h, GL_RGBA);
  free(pixels2);
}

void addImage(ofxCvGrayscaleImage & image_base, ofxCvGrayscaleImage & image_plus, int mode){
  if (mode == 0){
    unsigned char* pixels1 = image_base.getPixels();
    unsigned char* pixels2 = image_plus.getPixels();
    
    for (int j=0; j < image_base.height*image_base.width; j++){
      pixels1[j] = max(pixels1[j], pixels2[j]);
    }
  }
}

void addColorImage(ofImage & image_base, ofxCvGrayscaleImage & image_plus, int color){
  int r = 0;
  int g = 0;
  int b = 0;
  unsigned char* pixels1 = image_base.getPixels();
  unsigned char* pixels2 = image_plus.getPixels();
  hsv2rgb(color, r, g, b);
  for (int j=0; j < image_base.height*image_base.width; j++){
    int gris = pixels2[j];
    if (gris > 10){
      pixels1[j*4+0] = r*gris/255;
      pixels1[j*4+1] = g*gris/255;
      pixels1[j*4+2] = b*gris/255;
      pixels1[j*4+3] = 255;
    }
  } 
}

// void insertImage(ofxCvGrayscaleImage & image, int pos_x, int pos_y, int width, int height){
//   ofxCvGrayscaleImage image_resize = image;
//   image_resize.resize(width, height);
  
//   unsigned char* pixels_screen = screen.getPixels();
//   unsigned char* pixels_image = image.getPixels();
  
//   int w = width;
//   int h = height;
  
//   for (int i = 0; i<w; i++){
//     for (int j = 0; j < h; j ++){
//       int i_pix_screen = screen_rect.width*(j+pos_y) + pos_x + i;
//       if (i_pix_screen < screen_rect.height*screen_rect.width){
// 	pixels_screen[i_pix_screen] = max(pixels_image[h*j+i], pixels_screen[i_pix_screen]);
//       }
//     }
//   }
// }

void gaussian_elimination(float *input, int n)
{
    // ported to c from pseudocode in
    // http://en.wikipedia.org/wiki/Gaussian_elimination

    float * A = input;
    int i = 0;
    int j = 0;
    int m = n-1;
    while (i < m && j < n)
    {
        // Find pivot in column j, starting in row i:
        int maxi = i;
        for(int k = i+1; k<m; k++)
        {
            if(fabs(A[k*n+j]) > fabs(A[maxi*n+j]))
            {
                maxi = k;
            }
        }
        if (A[maxi*n+j] != 0)
        {
            //swap rows i and maxi, but do not change the value of i
            if(i!=maxi)
                for(int k=0; k<n; k++)
                {
                    float aux = A[i*n+k];
                    A[i*n+k]=A[maxi*n+k];
                    A[maxi*n+k]=aux;
                }
            //Now A[i,j] will contain the old value of A[maxi,j].
            //divide each entry in row i by A[i,j]
            float A_ij=A[i*n+j];
            for(int k=0; k<n; k++)
            {
                A[i*n+k]/=A_ij;
            }
            //Now A[i,j] will have the value 1.
            for(int u = i+1; u< m; u++)
            {
                //subtract A[u,j] * row i from row u
                float A_uj = A[u*n+j];
                for(int k=0; k<n; k++)
                {
                    A[u*n+k]-=A_uj*A[i*n+k];
                }
                //Now A[u,j] will be 0, since A[u,j] - A[i,j] * A[u,j] = A[u,j] - 1 * A[u,j] = 0.
            }

            i++;
        }
        j++;
    }

    //back substitution
    for(int i=m-2; i>=0; i--)
    {
        for(int j=i+1; j<n-1; j++)
        {
            A[i*n+m]-=A[i*n+j]*A[j*n+m];
            //A[i*n+j]=0;
        }
    }
}

void findHomography(ofPoint src[4], ofPoint dst[4], float homography[16])
{

    // create the equation system to be solved
    //
    // from: Multiple View Geometry in Computer Vision 2ed
    // Hartley R. and Zisserman A.
    //
    // x' = xH
    // where H is the homography: a 3 by 3 matrix
    // that transformed to inhomogeneous coordinates for each point
    // gives the following equations for each point:
    //
    // x' * (h31*x + h32*y + h33) = h11*x + h12*y + h13
    // y' * (h31*x + h32*y + h33) = h21*x + h22*y + h23
    //
    // as the homography is scale independent we can let h33 be 1 (indeed any of the terms)
    // so for 4 points we have 8 equations for 8 terms to solve: h11 - h32
    // after ordering the terms it gives the following matrix
    // that can be solved with gaussian elimination:

    float P[8][9]=
    {
        {-src[0].x, -src[0].y, -1, 0, 0, 0, src[0].x*dst[0].x, src[0].y*dst[0].x, -dst[0].x }, // h11
        { 0, 0, 0, -src[0].x, -src[0].y, -1, src[0].x*dst[0].y, src[0].y*dst[0].y, -dst[0].y }, // h12

        {-src[1].x, -src[1].y, -1, 0, 0, 0, src[1].x*dst[1].x, src[1].y*dst[1].x, -dst[1].x }, // h13
        { 0, 0, 0, -src[1].x, -src[1].y, -1, src[1].x*dst[1].y, src[1].y*dst[1].y, -dst[1].y }, // h21

        {-src[2].x, -src[2].y, -1, 0, 0, 0, src[2].x*dst[2].x, src[2].y*dst[2].x, -dst[2].x }, // h22
        { 0, 0, 0, -src[2].x, -src[2].y, -1, src[2].x*dst[2].y, src[2].y*dst[2].y, -dst[2].y }, // h23

        {-src[3].x, -src[3].y, -1, 0, 0, 0, src[3].x*dst[3].x, src[3].y*dst[3].x, -dst[3].x }, // h31
        { 0, 0, 0, -src[3].x, -src[3].y, -1, src[3].x*dst[3].y, src[3].y*dst[3].y, -dst[3].y }, // h32
    };

    gaussian_elimination(&P[0][0],9);

    // gaussian elimination gives the results of the equation system
    // in the last column of the original matrix.
    // opengl needs the transposed 4x4 matrix:
    float aux_H[]= { P[0][8],P[3][8],0,P[6][8], // h11 h21 0 h31
                     P[1][8],P[4][8],0,P[7][8], // h12 h22 0 h32
                     0 , 0,0,0, // 0 0 0 0
                     P[2][8],P[5][8],0,1
                   }; // h13 h23 0 h33

    for(int i=0; i<16; i++) homography[i] = aux_H[i];
}
