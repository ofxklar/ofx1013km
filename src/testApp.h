#pragma once

#include <stack>
#include <queue>

#include "math.h"
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxKinect.h"
#include "ofxOsc.h"
#include "utils.h"
#include "guiCustom.h"

#include "LoopRecorder.h"
#include "LoopRecorderNum.h"
#include "nakedStairs.h"
#include "k2mNote.h"

#include "screenFrame.h"
#include "screenFrameTex.h"
#include "multiFrame.h"

#include "delayParticles.h"

//#include "aubioAnalyzer.h"
#include "ofxMidi.h"
#include "ofxControlPanel.h"

//#include "AppCore.h"

#define PORT 12345
#define NUM_MSG_STRINGS 20

/* typedef struct { */
/* 	ofPoint pt; */
/* 	bool 	bBeingDragged; */
/* 	bool 	bOver; */
/* 	float 	radius; */
/* }draggableVertex; */

class testApp : public ofBaseApp {
 public:
  void setup();
  void update();
  void draw();
  void exit();
  
  void keyPressed  (int key);
  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);
  void windowResized(int w, int h);

  ofxOscReceiver   oscReceiver;

  void setupGui();
  void setupIndus();
  void setupK2mNote();

  void updateOsc();
  void updateGui();

  void updateKinect();
  void updateZoom();
  void updateK2m();
  void updateFlash();
  void updateColorFlash();
  void updateIndusBf();
  void updateIndusLoop();
  void updateIndus();
  void updateDelay(ofxCvGrayscaleImage & image_sortie, LoopRecorder & rec);
  void updateNakedStairs();

  void updateThreshold();
  void drawK2m();
  void panicK2m();

  void drawNakedStairs();
  void drawAlpha();

  void drawNsTest();
  void drawPointCloud();
  void drawMonitor();
  void drawIndustrie();
  void drawIndustrieMonitor(int pos_x, int pos_y, float ratio);
  void drawDelayMonitor(int x, int y, float ratio);
  void drawBuffer(int x, int y, int w, LoopRecorder &rec);
  void drawSoundMonitor(int x, int y, int w, int h, const queue<float>& p);
  void drawWarpPerspective();
  
  void indusSetRecord(int i);
  void indusKnob(int i, int val);
  void indusKnobButton(int i);
  void indusButton(int i);
  void indusStepUp(int i);
  void indusStepDown(int i);

  void eventsIn(guiCallbackData & data);
  void grabBackgroundEvent(guiCallbackData & data);

  ofxControlPanel gui;
  bool mode_gui_draw;
  simpleLogger logger;
  simpleLogger midi_logger;
  char xml_name[256];
  bool mode_gui_control;
  float elapsedTime;
  int appFrameCount;
  float appFrameRate;
		

  bool mode_kinect;
  ofxKinect kinect;
  ofxCvColorImage   colorImg;
  ofxCvColorImage   colorImg_vid;
  
  ofVideoGrabber    video;
  ofVideoGrabber   video2;

  ofxCvGrayscaleImage     grayDepth;
  ofxCvGrayscaleImage 	grayImage;

  ofxCvGrayscaleImage     grayDepth_vid;
  ofxCvGrayscaleImage 	grayImage_vid;

  
  ofxCvContourFinder 	contourFinder;
  ofRectangle cf_roi;
  ofxCvGrayscaleImage sortie_cf;


  ofxCvGrayscaleImage screen;
  
  // midi ///////////////////////////////////////////////////////////////////
  void newMidiMessage(ofxMidiMessage& args);
  ofxMidiIn midiIn;
  ofxMidiOut midiOut;

  void audioReceived(float * input, int bufferSize, int nChannels);
  void audioRequested(float * output, int bufferSize, int nChannels);
  
  //AppCore core;


  // TEST SON ///////////////////////////////////////////////////////////////
  //void audioReceived(float * input, int bufferSize, int nChannels);
		
  float * left;
  float * right;
  //aubioAnalyzer AA;
  stack<int> liste_pitch;
  queue<float> liste_amp;
  float   liste[10];
  float   seuil_son;
  float   son_val;
  float   mean_pitch;
  float   old_mean_pitch;
  int     sound_buffer;
  int     max_amp;
  int     buff_size;
  int     buff;
  int     pitch_max;
  int     pitch_min;
  int     pitch_r;
  int     pitch_g;
  int     pitch_b;
  int     pitch_color;
  int     seuil_niveau;
  bool    impulse;
  int     n_impulse;
  bool    impulse_monitor;
  float   impulse_value;
  // input //////////////////////////////////////////////////////////////////
  int     input_width;
  int     input_height;
  int     input_width_vid;
  int     input_height_vid;
  int     numPixels;
  bool is_frame_new;
  bool is_frame_new_kinect;
  bool is_frame_new_video;

  // reglage ecran //////////////////////////////////////////////////////////
  ofRectangle   screen_rect;
  float   screen_ratio;
  int     crop_left;
  int     crop_right;
  int     crop_up;

  ofRectangle screen_bis;
  float screen_bis_ratio;

  bool    draw_point_screen;

  bool    init_ecran;
  ofRectangle screen_init;
  float   screen_ratio_init;
  int     crop_left_init;
  int     crop_right_init;
  int     crop_up_init;

  bool    save_screen;
  bool    bak_screen;
  ofRectangle screen_bak;
  float   screen_ratio_bak;
  int     crop_left_bak;
  int     crop_right_bak;
  int     crop_up_bak;

  ofRectangle ns_screen;
  float   ns_screen_ratio;
  bool    ns_draw_point_screen;
  bool    ns_corner_from_bak;
  int     ns_input;

  bool mode_masque_bord;

  bool    mode_warp_perspective;
  bool    set_vertical; 
  ofPoint up_left_corner;
  ofPoint up_right_corner;
  ofPoint down_left_corner;
  ofPoint down_right_corner;

  int n_wrap_vertices;
  draggableVertex perspectiveVertices[16];
  
  // screen frame
  ScreenFrameTex frame1;
  MultiFrame multi;
  
  bool mode_multiframe_delay;
  bool multiframe_draw_image;
  bool multiframe_draw_frame;
  int multiframe_delay;
  bool multiframe_grid;
  int multiframe_num_frames;

  // monitor
  int     pos_x_monitor;
  int     pos_y_monitor;
  int     width_monitor;
  int     height_monitor;
    
  // Kinect
  int     angle;
  int   kinect_blur;
  int   kinect_dilate;
  int   kinect_erode;
  bool    correction_20;
  ofPoint kinect_20_up_left;
  ofPoint kinect_20_up_right; 
  ofPoint kinect_20_down_right; 
  ofPoint kinect_20_down_left;

  // seuil
  int 	  near_threshold;
  int	  far_threshold;
  int     near_range;
  int     far_range;
  int     pointCloudRotationY;

  //ofTrueTypeFont smallFont;
  
  // Video
  bool                    mode_live;
  bool                    mode_video_autocontrast;
  int                     height;
  int                     width;
  float                   video_contrast;
  float                   video_brightness;


  // OpenCV
  ofxCvGrayscaleImage 	grayBg;
  ofxCvGrayscaleImage 	grayDiff;
  ofxCvGrayscaleImage 	grayDiff2;
  ofxCvGrayscaleImage 	grayDiff3;

  bool                    bNewFrame;
  bool                    bLearnBakground;
  bool                    bLearnBakground_vid;
  int                     threshold;
  ofxCvGrayscaleImage     sortie;
  ofTexture               sortie_trans;
  ofxCvGrayscaleImage     sortie_vid;
  ofxCvGrayscaleImage     sortie_warp_vid;
  
  bool                    frameByframe;
  //ofxCvContourFinder 	    contourFinder;

  ofxCvGrayscaleImage 	grayBg_vid;
  ofxCvGrayscaleImage 	grayDiff_vid;

  bool mode_trans;
  // Global effet
  int     n_vids;
  int     delay;
  int     buffer_lenght;
  int     noir_fondu;
  
  int     mode_depth;
  bool    mode_threshold;
  int     mode_depth_vid;
  
  bool    mode_zoom;
  bool    mode_k2m;
  bool    mode_video;
  bool    mode_bg_kinect;
  bool    mode_bg_vid;
  bool    mode_classic_draw;
  bool    mode_naked_stairs_draw;
  bool    mode_alpha;
  
  // Boucles
  LoopRecorder            recorder;
  LoopRecorder            recorder_bis;
  LoopRecorder            recorder_vid;
  LoopRecorderNum         loops[9];
  ofxCvGrayscaleImage     loop_output[9];
  
  // industrie
  int indus_input;
  bool indus_turn_mode;
  int indus_turn_mode_step;

  LoopRecorder indus_recorder[9];
  bool    mode_industrie;

  ofPoint indus_monitor;
  float   indus_monitor_ratio;

  bool    indus_mode_grid;
  bool    indus_mode_grid_switch;
  bool    indus_mode_rond;
  bool    indus_mode_black;

  int     indus_buffer_lenght;
  ofTexture  indus_output[9];
  ofPoint indus_pos[9];
  float   indus_ratio[9];
  int     indus_angle[9];
  int     indus_mirror[9];
  bool     indus_mode_live[9];

  ofImage rond_im;
  ofImage bord_im;
  ofTexture bord_tex;
  ofxCvGrayscaleImage rond_gray;
  bool    indus_set_frame[9];
  float   indus_set_frame_pos[9];
  float   indus_new_ratio[9];
  int     indus_new_angle_i[9];
  float   indus_size_factor[9];
  bool    indus_play[9];
  bool    indus_set_pause[9];
  int    indus_layer[9];
  int     indus_new_vid;
  ofPoint indus_new_pos;
  int     indus_new_angle;
  int     indus_new_mirror;
  bool    indus_sync_loop;
  int     indus_sync_lenght;
  bool    indus_sync_set_stop[9];
  bool    indus_mode_size;
  int     indus_learn_size[9];
  float   indus_size_bak[9][400];
  float   indus_sync_current_n_loop[9];
  int     indus_start_master_loop;
  float     indus_sync_lenght_ms;
  bool    indus_hold;

  // Delay
  int     delay_input;
  int     compteur_delay[25];
  bool    bool_delay[25];
  int     delay_frame[25];
  int     n_delay;
  int     delay_lenght;
  bool    mode_delay_fixe;
  bool    mode_delay_gray;
  bool    mode_delay_end;
  bool    mode_delay_impulse;
  int     delay_decrease_value;
  int     delay_tap;
  int     delay_tap_n;
  int     delay_learn_tap;
  int     delay_offset;
  bool    mode_delay_nano;
  LoopRecorderNum         delay_recorder;
  ofxCvGrayscaleImage     image_delay;

  // Flash
  bool mode_flash;
  int     bool_flash;
  bool    mode_flash_impulse;
  ofxCvGrayscaleImage     sortie_flash;
  
  // color flash
  ofImage sortie_color_flash;
  int color_flash;

  // naked stairs
  NakedStairs nakedStairs;
  int     n_naked_stairs;
  int     naked_stairs_delay;
  int     naked_stairs_delay_255;
  int     ns_old_delay;
  int     ns_new_delay;
  int     ns_lst_delay[64];
  int     ns_delay;

  ofxCvGrayscaleImage   ns_gray[49];
  ofTexture             ns_rgba[49];
  int     ns_transition[6];
  int     ns_pos_x_init;
  int     ns_size;
  int     ns_width;
  int     ns_height;
  int     ns_nvids;
  int     ns_num;
  
  // texture
  ofTexture texture_test_r;
  ofTexture texture_test_g;
  ofTexture texture_test_b;
  int     decale_alpha1;
  int     decale_alpha2;

  //sortie couleur test
  ofxCvColorImage         sortie_color_test;
  bool                    mode_test;  

  // son ??
  /* ofSoundStream oFsoundStream; */
  /* ofSoundPlayer   son1; */
  /* ofSoundPlayer   son2; */
  
  stringstream    ss;
  string          filename;		

  // m2k
  ofTexture k2m_image;
  
  //bool k2m_clean;
  //bool k2m_send[24];
  int k2m_channel;
  int k2m_row;
  int k2m_col;
  int k2m_n_note;
  int k2m_bn1;
  K2mNote k2m_notes[40];
  //int k2m_bn2;
  ofxCvGrayscaleImage image_delay_ms1;
  ofxCvGrayscaleImage image_delay_ms2;
  ofxCvGrayscaleImage image_delay_ms3;
  int delay_ms;
  
  // test partices
  //ofxParticleManager particleManager;
ofPoint part_vit;
ofPoint part_pos;
 DelayParticles delay_particles;
};
