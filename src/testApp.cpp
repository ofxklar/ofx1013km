#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup() {
  //ofSetFrameRate(60);

  // midi
  //midiIn.openPort();
  //ofAddListener(midiIn.newMessageEvent, this, &testApp::newMidiMessage);
  //midiIn.addListener(&testApp::newMidiMessage);

  cout << "midi out" << endl;
  midiOut.listPorts();
  midiOut.openPort(1);

  //osc	
  cout << "listening for osc messages on port " << PORT << "\n";
  oscReceiver.setup( PORT );
  
  // reglage ecran base
  init_ecran = false;
  screen_init.set(0, 0, 1024, 768);
  screen_ratio_init = 1;
  crop_right_init = 0;
  crop_left_init = 0;
  crop_up_init = 0;

  screen.allocate(screen_rect.width, screen_rect.height);

  screen_rect.set(0,0, 1024, 768);
  screen_ratio = 1;
  crop_right = 0;
  crop_left = 0;
  crop_up = 0;

  screen_bis.set(0, 0, 1024, 768);
  screen_bis_ratio = 1;

  noir_fondu = 0;
  draw_point_screen = true;

  // pre-reglage (repetition)
  save_screen = false;
  bak_screen = false;
  screen_bak.set(0,0, 1024, 768);
  screen_ratio_bak = 1;
  crop_right_bak = 0;
  crop_left_bak = 0;
  crop_up_bak = 0;
  
  up_left_corner.set(0,0);
  up_right_corner.set(input_width,0);
  down_left_corner.set(0,input_height );
  down_right_corner.set(input_width, input_height);
  set_vertical = false;

    
  // kinect.init(true); //shows infrared image
  mode_kinect = false;
  mode_video = true;
  mode_trans = true;
  video_brightness = 0;
  video_contrast = 0;
  mode_video_autocontrast = false;
  mode_masque_bord = false;
  
  //bord_im.resize(1024, 768);

  mode_kinect = kinect.init(false, false, false);
  if (mode_kinect){
    cout << "mode kinect" << endl;
    //kinect.setVerbose(true);
    mode_kinect = kinect.open();
    input_width = kinect.width;
    input_height = kinect.height;
    numPixels = input_width*input_height;
    cout << "kinect w " << input_width << " h " << input_height << endl;
  }

  
  if (mode_video){

    //video.setVerbose(true);
    mode_video = video.initGrabber(640,480);
    input_width_vid = video.width;
    input_height_vid = video.height;
    cout << "input h/w " << input_width_vid << " " << input_height_vid << endl;
  }

  perspectiveVertices[0].pt.x= 0;
  perspectiveVertices[0].pt.y= 0;

  perspectiveVertices[1].pt.x = input_width_vid; //screen_rect.width;  
  perspectiveVertices[1].pt.y = 0;

  perspectiveVertices[2].pt.x = input_width_vid; // screen_rect.width;
  perspectiveVertices[2].pt.y = input_height_vid;//screen_rect.height;

  perspectiveVertices[3].pt.x = 0;
  perspectiveVertices[3].pt.y = input_height_vid;//screen_rect.height;


  for (int i = 0; i < 4; i++){
    perspectiveVertices[i].bOver = false;
    perspectiveVertices[i].bBeingDragged = false;
    perspectiveVertices[i].radius = 4;
  }


  // zero the tilt on startup
  angle = 20;
  kinect.setCameraTiltAngle(angle);
  kinect_blur = 0;
  correction_20 = true;
  kinect_20_up_left.set(55, 25);
  kinect_20_up_right.set(625, 25);
  kinect_20_down_right.set(640, 480);
  kinect_20_down_left.set(0, 480);
  
  // TEST SON
  // 0 output channels,
  // 2 input channels
  // 44100 samples per second
  // 256 samples per buffer
  // 4 num buffers (latency)
  
  //setup of sound input
  //soundStream.setDeviceID(2);

  //soundStream.listDevices();
  left = new float[256];
  right = new float[256];
  buff_size = 4;
  buff = 4;

  // sound_buffer = 100;
  // for (int i = 0; i < sound_buffer; i++){
  //   liste_amp.push(10);
  // }

  // AA.setup();
  seuil_son = 1;
  son_val = 0;
  mean_pitch = 0;
  max_amp = 0;
  old_mean_pitch = 0;
  pitch_min = 0;
  pitch_max = 500;
  pitch_r=0;
  pitch_g=0;
  pitch_b=0;
  seuil_niveau = 4;
  impulse = false;
  impulse_monitor = false;
  // start from the front
  pointCloudRotationY = 180;
  
  // monitor
  width_monitor = 320;
  height_monitor = 240;
  pos_x_monitor = 1024;
  pos_y_monitor = 0;
  
  mode_test = false;
  mode_classic_draw = false;
  mode_naked_stairs_draw = false;
  mode_depth = true;
  mode_threshold = false;
  mode_depth_vid = false;
  mode_alpha = false;
  mode_industrie = false;
 
  // seuil init
  near_range = 0;
  far_range = 255;
  near_threshold = 250;
  far_threshold  = 5;
  
  colorImg.allocate(input_width, input_height);
  grayDepth.allocate(input_width, input_height);
  grayImage.allocate(input_width, input_height);

    // OpenCV
  threshold = 50;
  mode_bg_kinect = false;
  grayBg.allocate(input_width, input_height);
  grayDiff.allocate(input_width, input_height);

  grayDiff2.allocate(input_width, input_height);
  grayDiff3.allocate(input_width_vid, input_height_vid);


  sortie.allocate(input_width, input_height);
  sortie_cf.allocate(input_width, input_height);
  //sortie_cf.setAnchorPercent(0.5, 1);

  mode_bg_vid = false;
  colorImg_vid.allocate(input_width_vid, input_height_vid);
  grayDepth_vid.allocate(input_width_vid, input_height_vid);
  grayImage_vid.allocate(input_width_vid, input_height_vid);
  //sortie_vid_bis.allocate(input_wIdth_vid, input_height_vid);

  grayBg_vid.allocate(input_width_vid, input_height_vid);
  grayDiff_vid.allocate(input_width_vid, input_height_vid);
  sortie_vid.allocate(input_width_vid, input_height_vid);
  sortie_warp_vid.allocate(input_width_vid, input_height_vid);
 
  mode_multiframe_delay = false;
  multiframe_draw_image = true;
  multiframe_draw_frame = false;
  multiframe_delay = 0;
  multiframe_grid = false;
  multiframe_num_frames = 25;
  multi.init(multiframe_num_frames);
  for (int i = 0; i < multiframe_num_frames; i ++){
    multi.setImage(i, sortie_vid);
    multi.loadXmlBak();

    //multi.setPos(i, i*5, 500);
    //multi.setGrid(input_width/2, input_height/2, 4, 4);
  }

  frame1.setImage(sortie_vid, OF_BLENDMODE_SCREEN);
  frame1.setAnchorPercent(0.5, 1);

  sortie_trans.allocate(input_width, input_height, GL_RGBA);
  k2m_image.allocate(input_width, input_height, GL_RGBA);
  bord_tex.allocate(input_width, input_height, GL_RGBA);
  bord_im.loadImage("bord.png");
  bord_im.resize(input_width, input_height);

  // Global effet
  delay = 10;
  n_vids = 9;
  buffer_lenght = 500;
  indus_buffer_lenght = 300; 
  mode_live = mode_kinect;
  
  // Boucles
  recorder.allocate(input_width, input_height, buffer_lenght, 0);
  recorder.setRecordModeOn();
  
  recorder_bis.allocate(input_width_vid, input_height_vid, buffer_lenght, 0);
  recorder_bis.setRecordModeOn();
  

  recorder_vid.allocate(input_width_vid, input_height_vid, buffer_lenght, 0);
  recorder_vid.setRecordModeOn();

  //indus_recorder(input_width, input_height, indus_buffer_lenght);
  //recorder.setRecordModeOff();
  indus_input = 1;
  setupIndus();

  // Delay
  delay_input = 0;
  n_delay = 0;
  delay_lenght = 0;
  delay_tap = 0;
  delay_offset = 0;
  delay_decrease_value = 1;
  mode_delay_fixe = false;
  mode_delay_nano = false;
  mode_delay_gray = false;
  mode_delay_end = false;
  mode_delay_impulse = false;
  image_delay.allocate(input_width, input_height);
  for (int i = 0; i < 25; i++){
    compteur_delay[i] = 0;
  }
  
  // Flash
  mode_flash = false;
  bool_flash = false;
  sortie_flash.allocate(input_width, input_height);
  mode_flash_impulse = false;
  sortie_color_flash.allocate(input_width, input_height, OF_IMAGE_COLOR_ALPHA);

  // naked Stairs
// reglage ecran ns
  ns_screen.set(0,0, 1024, 768);
  ns_screen_ratio = 1;
  ns_draw_point_screen = false;
  ns_nvids = 49;
  ns_input = 0;
  nakedStairs.init(ns_nvids, buffer_lenght, input_width, input_height);
   
  // texture
  texture_test_r.allocate(input_width, input_height, GL_RGBA);
  texture_test_g.allocate(input_width, input_height, GL_RGBA);
  texture_test_b.allocate(input_width, input_height, GL_RGBA);
  cout << " fin_init " << endl;

  // // kinect2midi
  // k2m_bn1 = 48;
  // k2m_bn2 = 96;
  // k2m_n_note = 18;
  // for (int i = 0; i < 2*k2m_n_note; i++){
  //   k2m_send[i] = false;
  // }

  // delay_ms_test
  image_delay_ms1.allocate(input_width_vid, input_height_vid);
  image_delay_ms2.allocate(input_width_vid, input_height_vid);
  image_delay_ms3.allocate(input_width_vid, input_height_vid);
  delay_ms = 450;

  setupGui();

  setupK2mNote();

 
  ofAddListener(gui.guiEvent, this, &testApp::eventsIn);	
  //soundStream.setup(this, 2, 2, 44100, , 4);
  //int ticksPerBuffer = 8;	// 8 * 64 = buffer len of 512
  //ofSoundStreamSetup(2, 2, this, 44100, ofxPd::blockSize()*ticksPerBuffer, 3);
  //core.setup(2, 2, 44100, ticksPerBuffer);
  //core.setDelayParticles(delay_particles);
}
 
void testApp::setupGui(){

  mode_gui_draw = false;
  // test ofxControlPanel
  ofxControlPanel::setBackgroundColor(simpleColor(0, 0, 0, 0));
  ofxControlPanel::setTextColor(simpleColor(255, 255, 255, 255));
  int x_dualscreen = 1024+340;
  int x_fullscreen = 0;
  //ofxControlPanel::setBackgroundColor(simpleColor(100, 100, 100, 100));
  gui.loadFont("MONACO.TTF", 8);		
  gui.setup("ofx1013km", x_dualscreen, 2, 3*1366/4, 755); // avant 1024 + 340

  gui.addPanel("  ", 4, true);

  ofxControlPanel::setBackgroundColor(simpleColor(100, 100, 100, 50));	
  gui.addPanel("Main Window", 4, false);

  ofxControlPanel::setBackgroundColor(simpleColor(100, 100, 100, 125));	
  gui.addPanel("System monitor", 3, false);

  ofxControlPanel::setBackgroundColor(simpleColor(100, 100, 100, 50));	
  gui.addPanel("Kinect2midi", 3, false);
  
  //--------- PANEL 1
  gui.setWhichPanel(1);
  
  gui.setWhichColumn(0);

  gui.addToggle("mode_gui_control", "MODE_GUI_CONTROL", false);
  gui.addToggle("mode_video", "MODE_VIDEO", mode_video);
  gui.addDrawableRect("video webcam", &colorImg_vid, 200, 150);
  gui.addDrawableRect("video webcam", &sortie_vid, 200, 150);
  gui.addToggle("autocontrast", "MODE_VIDEO_AUTOCONTRAST", false);
  gui.addSlider("brightness", "VIDEO_BRIGHTNESS", 0, -1, 1, false);
  gui.addSlider("contrast", "VIDEO_CONTRAST", 0, -1, 1, false);

  gui.setWhichColumn(1);
  gui.addToggle("mode_kinect", "MODE_KINECT", mode_kinect);
  gui.addDrawableRect("video kinect", &grayDiff, 200, 150);
  gui.addToggle("mode_depth", "MODE_DEPTH", true);
  gui.addToggle("mode_threshold", "MODE_THRESHOLD", false);
  gui.addSlider("near_threshold", "NEAR_THRESHOLD", 255, 255, 0, true);
  gui.addSlider("far_threshold", "FAR_THRESHOLD", 0, 255, 0, true);
  gui.addSlider("kinect_erode", "KINECT_ERODE", 2, 0, 10, true);
  gui.addSlider("kinect_dilate", "KINECT_DILATE", 2, 0, 10, true);
  gui.addSlider("kinect_blur", "KINECT_BLUR", 0, 0, 50, true);

  gui.setWhichColumn(2);
  gui.addDrawableRect("video sortie", &sortie, 200, 150);

  gui.addLabel(" /// flash");
  gui.addToggle("mode_flash_impulse", "MODE_FLASH_IMPULSE", false);

  gui.addLabel(" /// delay");
  vector <string> names2;
  names2.push_back("kinect");
  names2.push_back("video");
  gui.addTextDropDown("delay_input", "DELAY_INPUT", 0, names2);
  gui.addToggle("mode_delay_fixe", "MODE_DELAY_FIXE", 0);
  gui.addToggle("mode_delay_nano", "MODE_DELAY_NANO", 0);
  gui.addSlider("delay_lenght", "DELAY_LENGHT", 0, 0, 50, true);
  gui.addSlider("n_delay", "N_DELAY", 0, 0, 50, true);
  gui.addToggle("mode_delay_gray", "MODE_DELAY_GRAY", false);
  gui.addToggle("mode_delay_impulse", "MODE_DELAY_IMPULSE", false);

  gui.setWhichColumn(3);
  gui.addToggle("mode_live", "MODE_LIVE", 0);
  gui.addToggle("mode_classic_draw", "MODE_CLASSIC_DRAW", mode_kinect);

  gui.addLabel("/// industrie");
  vector <string> names3;
  names3.push_back("kinect");
  names3.push_back("video");
  gui.addTextDropDown("indus_input", "INDUS_INPUT", indus_input, names3);

  gui.addToggle("mode_industrie", "MODE_INDUSTRIE", false);		
  gui.addToggle("indus_mode_size", "INDUS_MODE_SIZE", false);
  gui.addToggle("indus_mode_rond", "INDUS_MODE_ROND", indus_mode_rond);
  gui.addToggle("indus_turn_mode", "INDUS_TURN_MODE", false);
  gui.addSlider("indus_turn_mode_step", "INDUS_TURN_MODE_STEP", 0, 0, 15, true);

  gui.addLabel("/// nakedStairs");
  gui.addToggle("mode_naked_stairs_draw", "MODE_NAKED_STAIRS_DRAW", 0);
  vector <string> names;
  names.push_back("kinect");
  names.push_back("video");
  gui.addTextDropDown("ns_input", "NS_INPUT", 0, names);
  gui.addSlider("n_naked_stairs", "N_NAKED_STAIRS", 0, 0, 8, true);
  gui.addSlider("naked_stairs_delay", "NAKED_STAIRS_DELAY", 0, 0, 5, true);


  gui.addLabel(" /// zoom");
  gui.addToggle("mode_zoom", "MODE_ZOOM", 0);

  gui.addLabel(" /// kinect2midi");
  gui.addToggle("mode_k2m", "MODE_K2M", 0);


  gui.setWhichPanel(2);
  gui.setWhichColumn(0);
  //some dummy vars we will update to show the variable lister object
  elapsedTime = ofGetElapsedTimef();
  appFrameCount	= ofGetFrameNum();	
  appFrameRate	= ofGetFrameRate();
  
  vector <guiVariablePointer> vars;
  vars.push_back( guiVariablePointer("ellapsed time", &elapsedTime, GUI_VAR_FLOAT, 2) );
  vars.push_back( guiVariablePointer("ellapsed frames", &appFrameCount, GUI_VAR_INT) );
  vars.push_back( guiVariablePointer("app fps", &appFrameRate, GUI_VAR_FLOAT, 2) );
  
  vars.push_back( guiVariablePointer("mouse x", &mouseX, GUI_VAR_INT) );
  vars.push_back( guiVariablePointer("mouse y", &mouseY, GUI_VAR_INT) );
  
  gui.addVariableLister("app vars", vars);
  
  gui.addChartPlotter("app fps", guiStatVarPointer("app fps", &appFrameRate, GUI_VAR_FLOAT, true, 2), 200, 100, 200, 0, 60);
  
  gui.setWhichColumn(1);
  gui.addLogger("events logger", &logger, 700, 300);
  gui.addLogger("midi logger", &midi_logger, 700, 300);

  gui.setWhichPanel(3);
  gui.setWhichColumn(0);
  gui.addSlider("k2m_bn1", "K2M_BN1", 48, 0, 127, true);
  //gui.addSlider("k2m_bn2", "K2M_BN2", 96, 0, 127, true);
  gui.addSlider("k2m_n_note", "K2M_N_NOTE", 12, 0, 24, true);
  gui.addLabel(" /// multiFrameDelay");
  gui.addToggle("mode_multiframe_delay", "MODE_MULTIFRAME_DELAY", 0);
  gui.addToggle("multiframe_draw_image", "MULTIFRAME_DRAW_IMAGE", 0);
  gui.addToggle("multiframe_draw_frame", "MULTIFRAME_DRAW_FRAME", 0);
  gui.addToggle("multiframe_grid", "MULTIFRAME_GRID", 0);
  gui.addSlider("multiframe_delay", "MULTIFRAME_DELAY", 0, 0, 20, true); 
  gui.addSlider("multiframe_num_frames", "MULTIFRAME_NUM_FRAMES", 16, 0, 25, true); 

  gui.addLabel(" /// multiFrameDelay_bis");
  gui.setupEvents();
  gui.enableEvents();
}

//this captures all our control panel events - unless its setup differently in testApp::setup
//--------------------------------------------------------------
void testApp::eventsIn(guiCallbackData & data){

  //lets send all events to our logger
  if( !data.isElement( "events logger" ) ){
    string logStr = data.getXmlName();
    
    for(int k = 0; k < data.getNumValues(); k++){
      logStr += " - " + data.getString(k);
    }
    
    logger.log(OF_LOG_NOTICE, "event - %s", logStr.c_str());
  }
  
  // print to terminal if you want to 
  //this code prints out the name of the events coming in and all the variables passed
  printf("testApp::eventsIn - name is %s - \n", data.getXmlName().c_str());
  if( data.getDisplayName() != "" ){
    printf(" element name is %s \n", data.getDisplayName().c_str());
  }
  for(int k = 0; k < data.getNumValues(); k++){
    if( data.getType(k) == CB_VALUE_FLOAT ){
      printf("%i float  value = %f \n", k, data.getFloat(k));
    }
    else if( data.getType(k) == CB_VALUE_INT ){
      printf("%i int    value = %i \n", k, data.getInt(k));
    }
    else if( data.getType(k) == CB_VALUE_STRING ){
      printf("%i string value = %s \n", k, data.getString(k).c_str());
    }
  }
  
  printf("\n");
}

void testApp::setupK2mNote(){
  cout << "init K2m" << endl;
  k2m_row = 12;
  k2m_col = 2;
  k2m_n_note = k2m_row * k2m_col;
  k2m_bn1 = 48;
  k2m_channel = 8;
  int w = input_width/4;
  int h = (input_height-40)/k2m_row;

  for (int i =0; i < k2m_col; i++){
    for (int j=0; j < k2m_row; j++){
      int n = j + i*k2m_row;
      int note = k2m_bn1 + n;
      ofRectangle key;
      key.set(20 + w*i, 20 + h*j, w, h);
      k2m_notes[n].init(midiOut, k2m_channel, note, key); 
    }
  }
  cout << "init K2M fin" << endl;
}

void testApp::setupIndus(){
  int input_w;
  int input_h;
  if (indus_input == 0){
    input_w = input_width;
    input_h = input_height;
  }else if (indus_input == 1){
    input_w = input_width_vid;
    input_h = input_height_vid;
  }
  for (int i = 0; i < n_vids; i++){
    indus_monitor.set(1024 + 370, 70);
    indus_monitor_ratio = 0.7;

    loops[i].setBufferLenght(buffer_lenght);
    indus_recorder[i].allocate(input_w, input_h, indus_buffer_lenght, 0);
    loop_output[i].allocate(input_w, input_h);
    indus_pos[i].set(screen_rect.width/2,screen_rect.height/2);
    indus_ratio[i] = 1;
    indus_new_ratio[i] = 1;
    indus_size_factor[i] = 1;
    indus_play[i] = false;

    indus_layer[i] = i;
    
    indus_angle[i] = 0;
    indus_new_angle_i[i] = 0;
    indus_mirror[i] = 0;
    indus_output[i].allocate(input_w, input_h, GL_RGBA);
    if (indus_input == 1){
      indus_output[i].setAnchorPercent(0.50,0.50);
    }else if (indus_input == 0){
      indus_output[i].setAnchorPercent(0.50, 1);
    }
    indus_set_frame[i] = false;
    indus_set_frame_pos[i] = 0;
    indus_set_pause[i] = false;
  
    indus_sync_set_stop[i] = false;
    indus_learn_size[i] = -1;
    indus_sync_current_n_loop[i] = 0;
    indus_mode_live[i] = false;
  }
  //rond_im.setUseTexture(false);
  //rond_im.setImageType(OF_IMAGE_GRAYSCALE);
  rond_im.loadImage("rond.png");
  rond_im.resize(input_w, input_h);

  indus_mode_grid  = false ;
  indus_mode_grid_switch = false;
  indus_mode_rond = true;
  indus_mode_size = false;
  indus_sync_loop = false;
  indus_sync_lenght = -1;
  indus_new_mirror = 0;
  indus_new_angle = 0;
  indus_new_vid = -1; 
  indus_new_pos.set(screen_rect.width/2,screen_rect.height/2);
  indus_hold = false;
}

// void updateIndusBf(){
//   for (int i = 0; i < n_vids; i++){
//     indus_recorder[i].getLoopEnd();
//   }
// }

void testApp::updateGui(){
  gui.update();
  mode_gui_control = gui.getValueI("MODE_GUI_CONTROL");
  mode_video = gui.getValueI("MODE_VIDEO");
  mode_video_autocontrast = gui.getValueI("MODE_VIDEO_AUTOCONTRAST");
  video_brightness = gui.getValueF("VIDEO_BRIGHTNESS");
  video_contrast = gui.getValueF("VIDEO_CONTRAST");

  mode_kinect = gui.getValueI("MODE_KINECT");
  mode_depth = gui.getValueI("MODE_DEPTH");
  mode_threshold = gui.getValueI("MODE_THRESHOLD");
  kinect_erode = gui.getValueI("KINECT_ERODE");
  kinect_dilate = gui.getValueI("KINECT_DILATE");
  kinect_blur = gui.getValueI("KINECT_BLUR");

  mode_live = gui.getValueI("MODE_LIVE");
  mode_classic_draw = gui.getValueI("MODE_CLASSIC_DRAW");
  mode_industrie = gui.getValueI("MODE_INDUSTRIE");

  indus_mode_rond = gui.getValueI("INDUS_MODE_ROND");
  indus_mode_size = gui.getValueI("INDUS_MODE_SIZE");
  indus_turn_mode = gui.getValueI("INDUS_TURN_MODE");
  indus_turn_mode_step = gui.getValueI("INDUS_TURN_MODE_STEP");

  mode_naked_stairs_draw = gui.getValueI("MODE_NAKED_STAIRS_DRAW");
  nakedStairs.setNsnum(gui.getValueI("N_NAKED_STAIRS"));
  nakedStairs.setDelay(gui.getValueI("NAKED_STAIRS_DELAY"));
  ns_input = gui.getValueI("NS_INPUT");

  delay_input = gui.getValueI("DELAY_INPUT");
  mode_delay_fixe = gui.getValueI("MODE_DELAY_FIXE");
  mode_delay_nano = gui.getValueI("MODE_DELAY_NANO");

  if (mode_gui_control){
    near_threshold = gui.getValueI("NEAR_THRESHOLD");
    far_threshold = gui.getValueI("FAR_THRESHOLD");
    delay_lenght = gui.getValueI("DELAY_LENGHT");
    n_delay = gui.getValueI("N_DELAY");
  }  

  mode_zoom = gui.getValueI("MODE_ZOOM");
  mode_k2m = gui.getValueI("MODE_K2M");

  k2m_bn1 = gui.getValueI("K2M_BN1");

  mode_multiframe_delay = gui.getValueI("MODE_MULTIFRAME_DELAY");
  multiframe_draw_image = gui.getValueI("MULTIFRAME_DRAW_IMAGE");
  multiframe_draw_frame = gui.getValueI("MULTIFRAME_DRAW_FRAME");
  multiframe_grid = gui.getValueI("MULTIFRAME_GRID");
  multiframe_delay = gui.getValueI("MULTIFRAME_DELAY");
  multiframe_num_frames = gui.getValueI("MULTIFRAME_NUM_FRAMES");
}


//--------------------------------------------------------------
void testApp::update() {
  elapsedTime		= ofGetElapsedTimef();
  appFrameCount	= ofGetFrameNum();	
  appFrameRate	= ofGetFrameRate();

  updateOsc();
  updateGui();

  //core.update();

  int temp_indus_input = gui.getValueI("INDUS_INPUT");
  if (temp_indus_input != indus_input){
    setupIndus();
    indus_input = temp_indus_input;
  }

  if (mode_industrie){
    //    updateIndusBf();
    for (int i = 0; i < n_vids; i++){
      indus_recorder[i].getLoopEnd();
    }
  }

  if (init_ecran){
    screen_rect = screen_init;
    screen_ratio = screen_ratio_init;
    crop_left = crop_left_init;
    crop_right = crop_right_init;
    crop_up = crop_up_init;

    mode_warp_perspective = true;
    init_ecran=false;
  }

  if (bak_screen){
    screen_rect = screen_bak;
    screen_ratio = screen_ratio_bak;
    crop_left = crop_left_bak;
    crop_right = crop_right_bak;
    crop_up = crop_up_bak;
    bak_screen = false;
  }

  if (mode_alpha){
    ofBackground(255,245,245);
  }else{
    ofBackground(0, 0, 0);
  }  

  // float max_amplitude = 0;
  // float ampli = AA.amplitude*100;
  // if (liste_pitch.size() != buff && ampli > seuil_niveau){
  //   max_amplitude = max(max_amplitude, AA.amplitude*100);
  //   //liste_pitch.push((int)AA.pitch);
  //   cout << "amplitude " << max_amplitude << endl;
  //   impulse = true;
  // } 
  
  //max_amplitude = min(50., max_amplitude);
  // if(liste_pitch.size() == buff){
  //   for (int i; i < buff; i++){
  //     mean_pitch += liste_pitch.top();
  //     liste_pitch.pop();      
  //   }
  //   mean_pitch /= buff_size;
  //   //pitch_max = max(pitch_max, mean_pitch);
  //   //pitch_min = min(pitch_min, mean_pitch);
  // }

  //pitch_color = ofMap(mean_pitch, pitch_min, pitch_max, 0, 360);
  //hsv2rgb(pitch_color, pitch_r, pitch_g, pitch_b);
  
  is_frame_new = false ;
  is_frame_new_kinect = false;
  is_frame_new_video = false;

  if (mode_kinect){
    kinect.update();
    is_frame_new_kinect = kinect.isFrameNew();
    is_frame_new = is_frame_new_kinect;
  }
  
  if (mode_video){
    //noir_fondu = 255;
    video.update();
    is_frame_new_video = video.isFrameNew();
    is_frame_new = is_frame_new_video;
  }

  if (mode_video && is_frame_new_video){

    is_frame_new_video = false;
    colorImg_vid.setFromPixels(video.getPixels(),input_width_vid, input_height_vid);

    grayImage_vid = colorImg_vid;

    if (mode_bg_vid){
      if (bLearnBakground_vid == true){
	grayBg_vid = grayImage_vid;
	bLearnBakground_vid = false;
      }
      grayDiff_vid.absDiff(grayBg_vid, grayImage_vid);
      grayDiff_vid.threshold(threshold);
      if (mode_depth_vid){
	grayDiff_vid *= grayImage_vid;
      }
    }else{
      grayDiff_vid = grayImage_vid;
    }

    if (mode_video_autocontrast){
      grayDiff_vid.contrastStretch();
    }else{
      grayDiff_vid.brightnessContrast(video_brightness, video_contrast);
    }

    sortie_vid = grayDiff_vid;
    ofPoint pt0;
    ofPoint pt1;
    ofPoint pt2;
    ofPoint pt3;
    
    pt0 = -perspectiveVertices[0].pt;

    pt1.set(input_width_vid - (perspectiveVertices[1].pt.x - input_width_vid),
	    - perspectiveVertices[1].pt.y);

    pt2.set(2 * input_width_vid - perspectiveVertices[2].pt.x,
     	    2 * input_height_vid - perspectiveVertices[2].pt.y);
    pt3.set(- perspectiveVertices[3].pt.x,
     	    2 * input_height_vid - perspectiveVertices[3].pt.y);

    ofPoint warp_in[4];
    warp_in[0].set(0, 0);
    warp_in[1].set(input_width_vid, 0);
    warp_in[2].set(input_width_vid, input_height_vid);
    warp_in[3].set(0, input_height_vid);

    ofPoint warp_out[4];
    warp_out[0] = perspectiveVertices[0].pt;
    warp_out[1] = perspectiveVertices[1].pt;
    warp_out[2] = perspectiveVertices[2].pt;
    warp_out[3] = perspectiveVertices[3].pt;

    sortie_warp_vid.warpIntoMe(sortie_vid, warp_in, warp_out);

    if (recorder_vid.getRecordMode()){
      int tt = recorder_vid.grabFrame(sortie_warp_vid);
    }

    // if (mode_multiframe_delay){
    //   multi.setNumFrames(multiframe_num_frames);
    //   if (multiframe_grid){
    // 	multi.setGrid(screen_rect.width/5, screen_rect.height/5, 5, 5);
    //   }
    //   for (int i = 0; i<multiframe_num_frames; i ++){
    // 	int i_frame = recorder_vid.getCurrentFrameRecord()-i*multiframe_delay-1;

    // 	while (i_frame<0){
    // 	  i_frame = buffer_lenght + i_frame;
    // 	}
	
    // 	if (i_frame==0){
    // 	  i_frame = 1;
    // 	}
    
    // 	//multi.updateImage(i, sortie_vid);
    // 	ofxCvGrayscaleImage i_image = recorder_vid.getFrameN(i_frame);
    // 	multi.updateImage(i, i_image);
    //   }
    // }
    frame1.update(sortie_vid);

    image_delay_ms1 = recorder_vid.getFrameDelay(delay_ms);
    image_delay_ms2 = recorder_vid.getFrameDelay(4*delay_ms);
    image_delay_ms3 = recorder_vid.getFrameDelay(5*delay_ms);
    

    if (mode_industrie and indus_input == 1){
      updateIndusLoop();
      updateIndus();
    }

    if (ns_input == 1){
      nakedStairs.update();
    }

    if (delay_input == 1){
      if (mode_delay_nano or mode_delay_fixe){
	updateDelay(sortie_vid, recorder_vid);
	sortie_vid.mirror(0,1);
      }
    }
  }

  if(is_frame_new_kinect){    
    updateKinect();
  }
  
  delay_particles.update();
}

void testApp::updateKinect(){
  if (mode_kinect){
    is_frame_new_kinect = false;
    grayImage.setFromPixels(kinect.getDepthPixels(), input_width, input_height);
    grayImage.mirror(0,0);
    if (correction_20){
      grayImage.warpPerspective(kinect_20_up_left,
				kinect_20_up_right,
				kinect_20_down_right,
				kinect_20_down_left);
      
    }
    grayDepth = grayImage;
    
    unsigned char * pix = grayImage.getPixels();
    int numPixels = grayImage.getWidth() * grayImage.getHeight();

    for(int i = 0; i < numPixels; i++){
      if( pix[i] < near_threshold && pix[i] > far_threshold ){	  
	if (mode_depth){
	  pix[i] = 255 * pix[i]/(near_threshold - far_threshold) - 255 * far_threshold / (near_threshold - far_threshold);
	} else {
	  pix[i] = 255;
	}
	
      }else if (pix[i] >= near_threshold and !mode_threshold){
	pix[i] = 255;
      }else{
	pix[i] = 0;
      }
    }

    //updateThreshold();  
  
    /// Traitement image
    //grayImage.convertToRange(near_range, far_range);
    if (kinect_blur != 0){
      grayImage.blur(kinect_blur);
    }
    if (kinect_erode != 0){
      for (int ee = 0; ee < kinect_erode; ee++){
	grayImage.erode();
      }
    }
    if (kinect_dilate != 0){
      for (int dd = 0; dd < kinect_dilate; dd++){
	grayImage.dilate();
      }
    }
    
    if (mode_bg_kinect){
      if (bLearnBakground == true){
	grayBg = grayImage;
	bLearnBakground = false;
      }
      grayDiff.absDiff(grayBg, grayImage);
      grayDiff.threshold(threshold);
      grayDiff *= grayImage;
    }else{
      grayDiff = grayImage;
    }
    
    /// ajout dans le tampon
    if (recorder.getRecordMode()){
      int tt = recorder.grabFrame(grayDiff);
    }

    
    
    grayDiff2 = grayDiff;
    grayDiff2.scale(0.5, 0.5);
    
    grayDiff2.setROI( input_width_vid*0/2,input_height_vid*0/2 , input_width_vid, input_height_vid);
    
    grayDiff3.setFromPixels(grayDiff2.getRoiPixels(), input_width_vid, input_height_vid);
    grayDiff2.setROI(0,0, input_width, input_height);
    
    if (recorder_bis.getRecordMode()){
      int tt = recorder_bis.grabFrame(grayDiff3);
    }
  
    /// Effets
    if (mode_zoom){
      updateZoom();
    }
    
    if (mode_k2m){
      updateK2m();
    }
    // FIN OPENCVLOOP Laurent///////////////////////////////////////////
  }
  if ((ns_input == 0) and (mode_naked_stairs_draw)){
    nakedStairs.update();
  }
  
  if (mode_live){
    sortie = grayDiff;
    //setMasque(bord_im, grayDiff, bord_tex);
  }else{
    sortie.set(0);
  }
  

  // FLASH
  if (mode_flash){
    updateFlash();
  }

  // DELAY
  if (delay_input == 0){
    if (mode_delay_nano or mode_delay_fixe){
      updateDelay(sortie, recorder);
    }
  }

  // SORTIE
  if (mode_warp_perspective){
    sortie.warpPerspective(up_left_corner, up_right_corner, down_right_corner, down_left_corner);
  }

  
  if (mode_multiframe_delay){
    multi.setNumFrames(multiframe_num_frames);
    if (multiframe_grid){
      multi.setGrid(screen_rect.width/5, screen_rect.height/5, 5, 5);
    }
    for (int i = 0; i<multiframe_num_frames; i ++){
      int i_frame = recorder_bis.getCurrentFrameRecord()-i*multiframe_delay-1;
      
      while (i_frame<0){
  	i_frame = buffer_lenght + i_frame;
      }
      
      if (i_frame==0){
  	i_frame = 1;
      }
      
      //multi.updateImage(i, sortie_vid);
      ofxCvGrayscaleImage i_image = recorder_bis.getFrameN(i_frame);
      cout << " i_image h " << i_image.height <<  " i_image w " << i_image.width << endl;
      multi.updateImage(i, i_image);
    }
  }
  
  
  //update the cv image
  grayImage.flagImageChanged();
  
  if (mode_trans){
    cvBlackToAlpha(sortie, sortie_trans);   
  }

  if (mode_industrie and indus_input == 0){
     updateIndusLoop();
     updateIndus();
  }
}
 
void testApp::updateZoom(){
  int minx = 1000;
  int maxx = 0;
  contourFinder.findContours(grayDiff, 50, numPixels/2, 3, false);
  for (int b = 0; b < contourFinder.blobs.size(); b++){
    if (contourFinder.blobs[b].boundingRect.x<minx){
      minx = contourFinder.blobs[b].boundingRect.x;
    }
    if (contourFinder.blobs[b].boundingRect.x + contourFinder.blobs[b].boundingRect.width> maxx){
      maxx = contourFinder.blobs[b].boundingRect.x + contourFinder.blobs[b].boundingRect.width;
    }
  }
  int cf_width = maxx-minx;
  int cf_height = ofMap(cf_width, 0, input_width, 0, input_height);
  //cout << "roi "<< minx << " " << maxx << " " << cf_width << " " << cf_height<<endl;
  cf_roi.set(minx, input_height - cf_height, cf_width, cf_height);
  sortie_cf = grayDiff;
  sortie_cf.setAnchorPoint(0, 0);
}


void testApp::updateK2m()
{
  contourFinder.findContours(grayDiff, 50, numPixels/2, 3, false);
  for (int xx = 0; xx < k2m_n_note; xx ++){
    k2m_notes[xx].unLock();
  }

  for (int b = 0; b < contourFinder.blobs.size(); b++){
    int x_blob = contourFinder.blobs[b].centroid.x;
    int y_blob = contourFinder.blobs[b].centroid.y;
    int velocity = ofMap(contourFinder.blobs[b].area, 0, 200, 0, 127);
    for (int xx = 0; xx < k2m_n_note; xx ++){
      k2m_notes[xx].update(x_blob, y_blob, velocity);
    }
  } 
}

void testApp::panicK2m(){
  for (int i = 0; i<k2m_n_note; i++){
    k2m_notes[i].sendNoteOff();
  }
}

void testApp::updateThreshold(){
  unsigned char * pix1 = grayDepth.getPixels();
  unsigned char * pix2;
  pix2 = new unsigned char [numPixels*4];
  for (int i = 0; i<numPixels; i++){
    if (pix1[i] < near_threshold and pix1[i] > far_threshold){
      pix2[i*4] = pix1[i];
      pix2[i*4+1] = 0;
      pix2[i*4+2] = 0;
      pix2[i*4+3] = 255;
    }else if (pix1[i] <= far_threshold and pix1[i] > far_threshold - 10){
      pix2[i*4] = 0;
      pix2[i*4+1] = pix1[i];
      pix2[i*4+2] = 0;
      pix2[i*4+3] = 255;    
    }else{
      pix2[i*4] = pix1[i];
      pix2[i*4+1] =pix1[i];
      pix2[i*4+2] = pix1[i];
      pix2[i*4+3] = 255;
    }
  }
  k2m_image.loadData(pix2, input_width, input_height, GL_RGBA);
  free(pix2);
}

void testApp::drawK2m(){
  for (int i = 0; i<k2m_n_note; i++){
    k2m_notes[i].draw();
  }
}

void testApp::updateFlash(){
  
  if (mode_flash_impulse and impulse){
    bool_flash = true;
    impulse = false;
  } 
  
  if (bool_flash){
    addImage(sortie_flash, grayDiff, 0);
    bool_flash = false;
  }
  sortie_flash -= delay;
  addImage(sortie, sortie_flash, 0);
}

void testApp::updateColorFlash(){
  if (bool_flash){
    addColorImage(sortie_color_flash, grayDiff, color_flash);
    //sortie_color_flash -= delay;
  }
}

void testApp::updateDelay(ofxCvGrayscaleImage & image_sortie, LoopRecorder & rec){

  if (mode_delay_impulse){
    if (impulse){
      n_delay++;
      n_delay = min(n_delay, 20);
      impulse = false;
    }else{
      n_delay-=3;
      n_delay = max(n_delay, 0);
      //n_delay = max(n_delay, 0);
    }
  }

  // tap tempo delay fixe
  if (delay_learn_tap){
    delay_tap_n += 1;
    cout << "delay_learn_tap tap_n " << delay_tap_n << endl;
  }
  if (delay_tap_n >= 0 and !delay_learn_tap){
    delay_tap = delay_tap_n;
    delay_tap_n = -1;
    cout << "tap_n > 0;; delay_tap " << delay_tap << " tap_n " << delay_tap_n << endl;
  }
  
  // delay fixe
  if (mode_delay_fixe && !mode_delay_end){
    if (delay_tap !=0){
      delay_lenght = delay_tap;
      delay_tap = 0;
      cout << "mode_delay_fixe " << delay_tap << " " << delay_lenght << endl;
    }
    for (int i_delay=0; i_delay<n_delay; i_delay++){
      compteur_delay[i_delay] = i_delay * delay_lenght - delay_offset;
    }
  }
  
  // delay fin
  if (mode_delay_end){
    for (int i_delay = 1 ; i_delay<n_delay; i_delay++){
      if (compteur_delay[i_delay] > 0){
	compteur_delay[i_delay] --;
      }
    }
  } 

  int delay0 = 1;
  if (mode_live){
    delay0 = 0;
  }

  for (int i = delay0; i < n_delay; i++){
    if (compteur_delay[i]>0){
      int n_frame_delay = rec.getCurrentFrameRecord() - compteur_delay[i];
      while (n_frame_delay <= 0){
	n_frame_delay += buffer_lenght;
      }
      
      ofxCvGrayscaleImage image_delay = rec.getFrameN(n_frame_delay-1);
      if (mode_delay_gray){
	image_delay -= ofMap(i, 0, n_delay, 0, delay_decrease_value * 255);
      }
      addImage(image_sortie, image_delay, 0);
    }
  }  
}

void testApp::updateIndusLoop(){

  for (int i = 0; i < n_vids; i++){
    if (indus_recorder[i].getRecordMode() or indus_recorder[i].getDelayOn()){
      if (indus_input == 1){
	indus_recorder[i].grabFrame(grayDiff_vid);
      }else if (indus_input == 0){
	indus_recorder[i].grabFrame(grayDiff);
      }
    }
    if (i>0){
      float current_lenght = indus_recorder[i].getCurrentLenghtRecord();
      
      // si la boucle est plus longue que la bouche de sync
      if (indus_sync_lenght_ms > 0){
    
	// je calcule le nombre de loop qu'elle contient
	float n_loop =  current_lenght / indus_sync_lenght_ms;
       
	// si on a demande de stopper
	if (indus_sync_set_stop[i] !=0 && indus_sync_current_n_loop[i] == 0){
	  // je stocke le nombre de boucles
	  indus_sync_current_n_loop[i] = int(n_loop)+1;
	  indus_sync_set_stop[i] = false;
	  cout << "indus_sync!=0 n_loop " << n_loop << " " << indus_sync_current_n_loop[i] << endl;
	}

	//cout << indus_sync_current_n_loop[i] << " " << n_loop << endl;
	if ((n_loop >= indus_sync_current_n_loop[i]) &&  indus_sync_current_n_loop[i] > 0){
	  cout << "sync " << indus_sync_set_stop[i]<< endl;
	  //cout << "indus_n " << indus_sync_current_n_loop[i] << endl; 
	  //cout << ">1 " << indus_sync_current_n_loop[i] - n_loop << endl;
	  indus_recorder[i].setRecordModeOff();
	  indus_recorder[i].setLenghtMillis((indus_sync_current_n_loop[i])*indus_sync_lenght_ms);
	  cout << "set millis " << (indus_sync_current_n_loop[i]+1)*indus_sync_lenght_ms << endl;
	  indus_sync_current_n_loop[i] = 0;
	  indus_recorder[i].play();
	  indus_sync_set_stop[i] = false;
	}
      }
    }
    
    if (indus_recorder[i].getPlayMode() && !indus_mode_live[i]){    
      loop_output[i] = indus_recorder[i].getFrame();
      if (!indus_play[i]){
	indus_new_vid = i;
      }
      indus_play[i] = true;	
    }else if (indus_mode_live[i]){
      if (indus_input == 1){
	loop_output[i] = sortie_vid;
      }else if(indus_input == 0){
	loop_output[i] = sortie;
      }
    }
    
  }
}

void testApp::updateIndus(){


  if (indus_mode_grid){
    if (indus_mode_grid_switch){
      float rat = (screen_rect.width/3)/input_width;
      for (int i=0; i<3; i++){
	indus_pos[i].set(i*screen_rect.width/3 + screen_rect.width/6, screen_rect.height/6);
	indus_ratio[i] = rat;
      }
      for (int i=3; i<6; i++){
	indus_pos[i].set((i-3)*screen_rect.width/3 + screen_rect.width/6, 3*screen_rect.height/6);
	indus_ratio[i] = rat;
      }
      for (int i=6; i<9; i++){
	indus_ratio[i] = 0;
      }
      
      indus_mode_grid_switch=false;
    }
    for (int i = 0; i<6; i++){
      if (!indus_play[i]){
	loop_output[i] = sortie_vid;
      }
    }
  }
  
  for (int i=0; i<n_vids; i++){
    indus_ratio[i] = indus_new_ratio[i]*indus_size_factor[i];
    //indus_angle[i] = indus_new_angle_i[i];

    if (indus_turn_mode){
      indus_angle[i] += indus_turn_mode_step;
    }

    if (indus_learn_size[i] == 1){
      indus_size_bak[i][indus_recorder[i].getCurrentFrameNum()] = indus_ratio[i];
    }else{
      if (indus_learn_size[i] == 0){
	indus_ratio[i] = indus_size_bak[i][indus_recorder[i].getCurrentFrameNum()];
      }
    }

    if (indus_new_vid == i){
      if (! indus_mode_grid){
	
	if (indus_pos[i] != indus_new_pos){
	    indus_pos[i] += (indus_new_pos - indus_pos[i])/2 ;
	    //indus_pos[i].y = indus_pos[i].y + (indus_new_pos.y - indus_pos[i].y)/2 ;
	  }
	
	//indus_pos[i] = indus_new_pos;
      }
      indus_angle[i] = indus_new_angle;
    }

    if (indus_set_frame[i]){

      indus_recorder[i].setFrame(indus_set_frame_pos[i]);

      if (indus_play[i] && !indus_recorder[i].getPlayMode()){
	loop_output[i] = indus_recorder[i].getCurrentFramePlay();
      }else{
	cout << "probleme?" << endl;
	indus_set_frame[i] = false;
      }

    }
    if (indus_mode_rond){
      setMasque(rond_im, loop_output[i], indus_output[i]);
    }else {
      cvGrayToRgba(loop_output[i], indus_output[i], 1, 1, 1, true);   

    }
  }
}

//--------------------------------------------------------------
void testApp::draw() {
  //core.draw();
  ofSetColor(255, 255, 255);
  

  if (draw_point_screen){
    ofSetColor(100, 100, 100);
    ofCircle(screen_rect.x, screen_rect.y, 2);
    ofCircle(screen_rect.x + screen_init.x*screen_ratio, screen_init.x, 2);
    ofCircle(crop_left, screen_rect.y, 2);
    ofCircle(screen_init.width - crop_right, screen_rect.y, 2);
    ofSetColor(255,255,255);
  }


  ofPushMatrix();
    ofTranslate(screen_rect.x, screen_rect.y);
    ofScale(screen_ratio, screen_ratio);

    if (mode_industrie){
      drawIndustrie();
    }

    if (mode_zoom){
      float rat0 = screen_rect.width/cf_roi.width;
      sortie_cf.draw(-rat0 * cf_roi.x, -rat0 * (input_height - cf_roi.height), rat0 * input_width, rat0 * input_height);
    }
    
    if (mode_k2m){
      k2m_image.draw(0, 0, screen_rect.width, screen_rect.height);
      float rat = screen_rect.width/input_width;
      ofPushMatrix();
      ofScale(rat, rat);
      drawK2m();
      ofPopMatrix();
    }

    if (mode_alpha){
      drawAlpha();
    }
  
    // fondu sur ecran
    if (mode_classic_draw){
      if (delay_input == 1){
	sortie_vid.draw(0, 0, screen_rect.width, screen_rect.height);
      }else if (mode_trans){
	sortie_trans.draw(screen_bis.x, screen_bis.y, screen_bis.width, screen_bis.height);	
      }else{
	sortie.draw(0, 0, screen_rect.width, screen_rect.height);
	//sortie.drawROI(0,0);
	//sortie.resetROI();
      }
    }
    ofSetColor(0,0,0,noir_fondu);
    ofRect(0, 0, screen_rect.width, screen_rect.height);
  ofPopMatrix();

  // affichage crop
  ofSetColor(0,0,0);
  ofRect(0, 0, crop_left, screen_init.height);
  ofRect(screen_init.width - crop_right, 0, crop_right, screen_init.width);
  ofRect(0, 0, screen_init.width, crop_up);

  ofSetColor(255,255,255);
  if (mode_naked_stairs_draw and ns_input == 0){
    nakedStairs.draw(0, 0, screen_rect.width, screen_rect.height, recorder);
  } 
  if (mode_naked_stairs_draw and ns_input == 1){
    nakedStairs.draw(0, 0, screen_rect.width, screen_rect.height, recorder_vid);
  } 

  drawMonitor();
  //if (mode_warp_perspective){
  //sortie_warp_vid.draw(100,100);
  //drawWarpPerspective();
  //}

  ofEnableBlendMode(OF_BLENDMODE_SCREEN); 
  
  if (multiframe_delay){
    multi.draw(multiframe_draw_image, multiframe_draw_frame);
  }

  //frame1.drawImage();
  //frame1.drawFrame();

  ofDisableBlendMode();

  if (mode_masque_bord){
    //sortie.draw(0, 0, screen_rect.width, screen_rect.height);
    ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
    bord_im.draw(0,0,1024, 768);
    ofDisableBlendMode();
  }

  // sortie_vid.draw(0,0);
  // image_delay_ms1.draw(0, input_height_vid);
  // image_delay_ms2.draw(input_width_vid, 0);
  // image_delay_ms3.draw(input_width_vid, input_height_vid);
  
  //grayDiff2.drawROI(0,0);
  //bord_tex.draw(0,0, 1024, 768);

  delay_particles.draw();
  //core.draw_part();
  gui.draw();
  //ofSetColor(255,255,255);
}

void testApp::drawAlpha() {
  int h = texture_test_g.getHeight();
  int w = texture_test_g.getWidth();
  
  //texture_test_g.draw(1024/2 - w/2, 768 - h, w, h);
  texture_test_r.draw(1024/2 - w/2-decale_alpha1, 768-h, w, h);
  texture_test_b.draw(1024/2 - w/2+decale_alpha2, 768-h, w, h);
}

void testApp::drawIndustrie(){
  int h = screen_rect.height/3;
  int w = screen_rect.width/3;
  int nnn = n_vids;
  if (indus_mode_grid){
    nnn = 6;
  }
  for (int z = 0; z < nnn; z++){
    int i = indus_layer[z];
    
    if (indus_play[i] or indus_mode_grid or indus_mode_live[i]){
      ofPushMatrix();
      ofTranslate(indus_pos[i].x, indus_pos[i].y);
      ofRotate(indus_angle[i]);
      //loop_output[i].draw(0,0, w*indus_ratio[i], h*indus_ratio[i]);
      indus_output[i].draw(0,0, w*indus_ratio[i], h*indus_ratio[i]);
      ofPopMatrix();
    }
  }
}

void testApp::drawWarpPerspective(){
  
  ofNoFill();
  ofSetColor(255,0,0    );
  ofBeginShape();
    for (int j = 0; j<4; j++){
      ofVertex(perspectiveVertices[j].pt.x+100, perspectiveVertices[j].pt.y+100);
      if (perspectiveVertices[j].bOver == true) ofFill();
      else ofNoFill();
      ofCircle(perspectiveVertices[j].pt.x+100, perspectiveVertices[j].pt.y+100,4);
    }
  ofEndShape(true);
}

void testApp::drawIndustrieMonitor(int pos_x, int pos_y, float ratio){
  int h = screen_rect.height/3.;
  int w = screen_rect.width/3.;

  int w_i = screen_rect.width*ratio;
  int h_i = screen_rect.height*ratio;

  ofSetColor(50,50,50);
  ofRect(pos_x, pos_y, w_i, h_i);

  ofSetColor(200,200,200);
  ofNoFill();
  ofRect(pos_x, pos_y, w_i, h_i);
  ofFill();

  stringstream layer_monitor;

  for (int j = 0; j < n_vids; j++){
    int i = indus_layer[j];
    layer_monitor << i << "  " << indus_layer[i] << endl;
    if (indus_play[i] or indus_mode_live[i]){

      ofPushMatrix();
	ofTranslate(pos_x, pos_y);
	ofScale(ratio, ratio);
      
	ofTranslate(indus_pos[i].x, indus_pos[i].y);
	ofRotate(indus_angle[i]);

	ofSetColor(255,255,255);
	indus_output[i].draw(0,0, w*indus_ratio[i], h*indus_ratio[i]);
	ofPopMatrix();
    }
  }
  ofSetColor(255, 0, 0);
  ofDrawBitmapString(layer_monitor.str(), pos_x, pos_y);

  for (int i = 0; i < n_vids; i++){
    //recalage monitor

    if (indus_play[i]or indus_mode_live[i]){
	
      ofPushMatrix();
	ofTranslate(pos_x, pos_y);
	
	ofScale(ratio, ratio);
	
	ofTranslate(indus_pos[i].x, indus_pos[i].y);
	ofRotate(indus_angle[i]);
	
	stringstream indus_monitor_str;
	indus_monitor_str << i+1 << endl;
	  
	ofNoFill();

	if (indus_recorder[i].getPlayMode()){
	  ofSetColor(0,255,0);
	}else{
	  ofSetColor(200,200,200);
	}

	if (indus_recorder[i].getRecordMode()){
	  ofSetColor(255, 0,0);
	}else{
	  ofSetColor(200,200,200);
	}
	
	ofSetColor(255,255,255);
       
	ofScale(1/ratio, 1/ratio);
	ofRotate(-indus_angle[i]);
	ofSetColor(0,255,0);
	ofDrawBitmapString(indus_monitor_str.str(), 0,0);
	ofRotate(indus_angle[i]);
	ofScale(ratio, ratio);

	ofSetColor(255, 255, 255);
	if (indus_new_vid == i){
	  ofSetColor(0,0,255);
	};

	if (indus_input == 1){
	  ofRect(-indus_ratio[i]*w/2, -indus_ratio[i]*h/2, w*indus_ratio[i], h*indus_ratio[i]);
	}else if (indus_input == 0){
	  ofRect(-indus_ratio[i]*w/2, -indus_ratio[i]*h, w*indus_ratio[i], h*indus_ratio[i]);
	}
	ofSetColor(255,255,255);

      ofPopMatrix();
      
    }
  }
  for (int i = 0; i < n_vids; i++){
    ofPushMatrix();
    
      ofTranslate(pos_x, pos_y);
      ofScale(ratio, ratio);
    
      //recalage monitor
      ofSetColor(200, 200, 200);
      int pos_bande = ofMap(i, 0, n_vids, 0, screen_rect.width/2);
      int w_bande = screen_rect.width/(n_vids);
      drawFadderVert(pos_bande, 
		     screen_rect.height, 
		     indus_recorder[i].getLoopLenght() *5,
		     ofMap(indus_recorder[i].getPosition(), 0, indus_recorder[i].getLoopLenght()+1, 0, 255));
      
      ofPopMatrix();
  }
  
}


void testApp::drawSoundMonitor(int x, int y, int w, int h, const queue<float>& p){
  ofNoFill();
  ofPushMatrix();
  ofTranslate(x, y);
  ofRect(0, 0, w, h);
  queue<float> t = p;
  int compt = 0;
  ofSetColor(255, 255, 255);
  ofLine(0, h-10*seuil_son, w, h-10*seuil_son);

  while(!t.empty()){
    float valeur = t.front();
    ofRect(compt*3, h, 3, - valeur * 10);
    t.pop();
    compt++;
  }

  if (impulse and (mode_delay_impulse or mode_flash_impulse)){
    ofFill();
    ofSetColor(255, 0, 0);
    ofCircle(w/2, h/2, 50);
    ofNoFill();
    ofSetColor(255, 255, 255);
  }
  ofFill();
  ofPopMatrix();
}

void testApp::drawMonitor(){
  ofPushMatrix();
    ofTranslate(pos_x_monitor, pos_y_monitor);
    int w_mon = width_monitor;
    int h_mon = height_monitor;
    int x_col_2 = w_mon + 45 ;
    int x_col_3 = 2*w_mon + 90;
    
    ofSetColor(255,255,255);
    
    // 3 images de base : brut kinect, seuil et sortie
    grayDepth.draw(10, 10, w_mon/2, h_mon/2);

    sortie_vid.draw(10 + w_mon/2, 10, w_mon/2, h_mon/2);
    grayImage.draw(10, h_mon/2+10, w_mon/2, h_mon/2);
    sortie.draw(10         +screen_rect.x*width_monitor/screen_rect.width,  
		h_mon*2+30 +screen_rect.y*height_monitor/screen_rect.height, 
		w_mon*screen_ratio, 
		h_mon*screen_ratio);
    ofSetColor(0, 0, 255);

    ofNoFill();
    ofRect(10         +screen_rect.x*width_monitor/screen_rect.width,  
		h_mon*2+30 +screen_rect.y*height_monitor/screen_rect.height, 
		w_mon*screen_ratio, 
		h_mon*screen_ratio);    
    ofFill();
    ofNoFill();
    ofSetColor(0,255,0);
    ofRect(10         +screen_bak.x*width_monitor/screen_bak.width,  
	   h_mon*2+30 +screen_bak.y*height_monitor/screen_bak.height, 
	   w_mon*screen_ratio_bak, 
	   h_mon*screen_ratio_bak);    
    ofFill();

    //crop
    ofSetColor(255,0,0);
    ofLine(10+crop_left*w_mon/screen_rect.width, h_mon*2+30, 10+crop_left*w_mon/screen_rect.width, h_mon*3+30);
    ofLine(10+ w_mon-crop_right*w_mon/screen_rect.width, h_mon*2+30, 10+ w_mon-crop_right*w_mon/screen_rect.width, h_mon*3+30);
    ofLine(10,             h_mon*2+30+crop_up*w_mon/screen_rect.width, 
	   10 + w_mon,     h_mon*2+30+crop_up*w_mon/screen_rect.width) ;
    
    
    ofSetColor(255,255,255);
    ofNoFill();
    ofRect(10, 10, w_mon/2, h_mon/2);
    ofRect(10, h_mon/2+10,   w_mon/2,          h_mon/2);
    ofRect(10+w_mon/2, 10, w_mon/2, h_mon/2);
    ofRect(10+w_mon/2, h_mon/2+10,   w_mon/2,          h_mon/2);

    ofRect(10, 2*h_mon+30,   w_mon,          h_mon);
    
    ofFill();
    
    // faddr 
    drawFadder(10 , h_mon*3+30, w_mon, noir_fondu);

    // seuil kinect
    drawFadderVert(10, 10, h_mon/2, near_threshold);
    drawFadderVert(20, 10, h_mon/2, far_threshold);
    
    drawSwitch(w_mon/2-15, 15, mode_kinect);
    drawSwitch(w_mon/2-15, h_mon/2+15, mode_live);
    drawSwitch(w_mon-15, 2*h_mon+35, mode_classic_draw);
    
    // position ecran
    stringstream string_screen;
    string_screen << "x " << screen_rect.x << endl
		  << "y " << screen_rect.y << endl
		  << "r " << screen_ratio << endl;
    ofSetColor(0,0,255);
    ofDrawBitmapString(string_screen.str(), 20, h_mon*2 +45);

    stringstream string_screen_bak;
    string_screen_bak << "x " << screen_bak.x << endl
		      << "y " << screen_bak.y << endl
		      << "r " << screen_ratio_bak << endl;
    ofSetColor(0,255,0);
    ofDrawBitmapString(string_screen_bak.str(), 20, h_mon*2 + 90);

    // deuxieme colonne
    ofSetColor(255,255,255);

    drawBuffer(1650 - 1024, 5, 300, recorder);
    drawBuffer(2000 - 1024, 5, 300, recorder_vid);

    drawDelayMonitor(10, h_mon + 15, 1);

    ofSetColor(255,255,255);
    
    drawFadderVert(w_mon+20, h_mon*2, w_mon/2, ofMap(son_val, 0, 10, 0, 255));
    //drawSoundMonitor(10, h_mon + 20, w_mon, h_mon, liste_amp);
    contourFinder.draw(10,  h_mon + 20, w_mon, h_mon);
    ofNoFill();
    ofTranslate(10, h_mon + 20);
    //ofScale(w_mon/input_width,w_mon/input_width);
    ofRect(cf_roi.x, cf_roi.y, cf_roi.width, cf_roi.height);
    ofFill();
    ofPopMatrix();  


    if (mode_industrie){
      drawIndustrieMonitor(indus_monitor.x, indus_monitor.y, indus_monitor_ratio);
    }
}

void testApp::drawBuffer(int x, int y, int w, LoopRecorder &rec){

  int h = 14;
  int buffer_l = rec.getLenghtMax();
  int x_record = ofMap(rec.getCurrentFrameRecord(), 0, buffer_l, 0, w);
  int x_lenght = ofMap(rec.getLoopLenght(), 0, buffer_l, 0, w);
  if (x_lenght > w){
    x_lenght = w;
  }
  ofPushMatrix();
  ofTranslate(x, y);
  ofSetColor(100, 100, 100);
  ofFill();
  ofRect(0, 0, x_lenght, h);
  
  ofSetColor(170, 170, 170);
  ofNoFill();
  ofRect(0, 0, w, h);
  ofFill();
  ofLine(x_record, 0, x_record, h);
  
  stringstream name;
  name << "buffer (lenght: " << buffer_l << ")" << endl;
  ofDrawBitmapString(name.str(), 10, 12);
  stringstream no_frame;
  no_frame << rec.getCurrentFrameRecord();
  ofDrawBitmapString(no_frame.str(), w-50, 12);
  ofPopMatrix();
}

void testApp::drawDelayMonitor(int x, int y, float ratio){
  ofPushMatrix();
  ofTranslate(x, y);
    ofScale(ratio, ratio);

    ofSetColor(100, 100, 100);
    ofRect(0, 50, width_monitor, 15);
    ofSetColor(255, 255, 255);
    
    int nn;
    if (mode_delay_nano){
      nn = 5;
    } else if (mode_delay_fixe){
      nn = n_delay;
    } else {
      nn=0;
    }
    for (int i = 0; i < nn;i++){
      if (compteur_delay[i]!=0){
	int pos_x = ofMap(compteur_delay[i], 0, 300, 0, width_monitor);
	ofLine(pos_x, 48, pos_x, 67);
	stringstream no_vid;
	no_vid << i << endl
	       << compteur_delay[i];
	ofDrawBitmapString(no_vid.str(), pos_x, 65);
      }
    }
    ofPopMatrix();
}

void testApp::updateOsc(){
  n_impulse = 0;
  while( oscReceiver.hasWaitingMessages() ){
    // get the next message
    ofxOscMessage m;
    oscReceiver.getNextMessage( &m );
    if (m.getAddress() ==  "/indus/record"){
      int i_indus = m.getArgAsInt32(0);
      int record_mode = m.getArgAsInt32(1);
      if (record_mode == 0){
	indus_recorder[i_indus].setRecordModeOff();	
	indus_recorder[i_indus].play();
      }else if (record_mode == 1){
	indus_recorder[i_indus].setRecordModeOn();	
      }
      
      //cout << "posss :" << posss << endl;
    }else if (m.getAddress() == "indus/play"){
      int i_indus = m.getArgAsInt32(0);
      indus_recorder[i_indus].play();
    }else if (m.getAddress() == "indus/stop"){
      int i_indus = m.getArgAsInt32(0);
      indus_recorder[i_indus].stop();
    }else if (m.getAddress() == "/indus/pos"){
      int i_indus = m.getArgAsInt32(0);
      float indus_pos = m.getArgAsFloat(1);
      indus_recorder[i_indus].setFrame(indus_pos);
    }else if (m.getAddress() == "/impulse"){
      n_impulse ++;
      impulse = true;
      cout << "impulse" << endl;
    }
  }
}

//--------------------------------------------------------------
void testApp::keyPressed (int key) {
  bool control_panel_ate_key = gui.keyPressed( key );

  switch (key) {

  // case 'm':
  //   multi.saveToXmlBak();
  //   cout << "yeye"<< endl;
  //   break;

  // case 'l':
  //   multi.loadXmlBak();
  //   break;
   
  case 'I':
    
    part_pos.set(100,100);
    part_vit.set(100.0, 10.0);
    delay_particles.addParticle(part_pos, part_vit, 30, 100);
    
    break;
   

  case 'U':
    
    part_pos.set(100,100);
    part_vit.set(200.0,20.0);
    delay_particles.addParticle(part_pos, part_vit, 30, 100);
    break;
 
  case ' ':
    panicK2m();
    break;
    
  case 'O':
    n_naked_stairs++;
    if (n_naked_stairs>64){
      n_naked_stairs = 64;
    }
    break;
  case 'o':
    n_naked_stairs--;
    if (n_naked_stairs < 0){
      n_naked_stairs = 0;
    }
    break;
  case 'P':
    ns_new_delay ++;
    break;
  case 'p':
    ns_new_delay --;
    if (ns_new_delay < 0){
      ns_new_delay = 0;
    }
    break;
    
  case OF_KEY_UP:
    angle++;
    if(angle>30) angle=30;
    kinect.setCameraTiltAngle(angle);
    break;
    
   case OF_KEY_DOWN:
    angle--;
    if(angle<-30) angle=-30;
    kinect.setCameraTiltAngle(angle);
    break;
    // Commandes OpenCV
    
  // case ' ':
  //   bLearnBakground = true;
  //   break;
  case '*':
    threshold ++;
    if (threshold > 255) threshold = 255;
    break;
  case 'ù':
    threshold --;
    if (threshold < 0) threshold = 0;
    break;

    // commande industrie
  case 'a':
    indus_new_vid = 0;
    indus_new_pos = indus_pos[0];
    indus_new_angle = indus_angle[0];
    break;

  case 'z':
    indus_new_vid = 1;
   indus_new_pos = indus_pos[1];
    indus_new_angle = indus_angle[1];
    break;

  case 'e':
    indus_new_vid = 2;
   indus_new_pos = indus_pos[2];
    indus_new_angle = indus_angle[2];
    break;

  case 'r':
    indus_new_vid = 3;
   indus_new_pos = indus_pos[3];
    indus_new_angle = indus_angle[3];
    break;

  case 't':
    indus_new_vid = 4;
   indus_new_pos = indus_pos[4];
    indus_new_angle = indus_angle[4];
    break;

  case 'y':
    indus_new_vid = 5;
   indus_new_pos = indus_pos[5];
    indus_new_angle = indus_angle[5];
    break;

  case 'u':
    indus_new_vid = 6;
   indus_new_pos = indus_pos[6];
    indus_new_angle = indus_angle[6];
    break;

  case 'i':
    indus_new_vid = 7;
   indus_new_pos = indus_pos[7];
    indus_new_angle = indus_angle[7];
    break;
    
  case 'q':
    if (!indus_hold){
      if (!indus_recorder[0].getRecordMode()){
	midiOut.sendNoteOn(7, 10, 127);
	indus_recorder[0].setRecordModeOn();
	indus_start_master_loop = ofGetElapsedTimeMillis();
      }else{
	
	if (indus_sync_loop){
	  midiOut.sendNoteOff(7, 10, 0);	
	  indus_recorder[0].setRecordModeOff();
	  indus_sync_lenght = indus_recorder[0].getLoopLenght();
	  indus_sync_lenght_ms = indus_recorder[0].getLoopLenghtMillis();
	  indus_recorder[0].play();
	}
      }
    }    
    break;
  case 's':
    indusSetRecord(1);
    break;
  case 'd':
    indusSetRecord(2);
    break;
  case 'f':
    ofToggleFullscreen();
    break;
  case 'g':
    indusSetRecord(4);
    break;
  case 'h':
    indusSetRecord(5);
    break;    
  case 'j':
    indusSetRecord(6);
    break;    

  // case 'g':
  //   mode_gui_draw = !mode_gui_draw;
  //   break;
    
    // Commande flash
  case OF_KEY_RETURN:
    // bool_flash[n_flash] = true;
    // n_flash++ ;
    indus_new_vid = -1;
    indus_new_angle = 0;
    break;

  case ':':
    delay --;
    if (delay < 0){
      delay = 0;
    }
    break;

  case '!':
    delay ++;
    break;
  case '/':
    delay = 0;
    break;
    // Commande naked stairs
    
  case 'V':
    n_naked_stairs ++;
    break;
   
  }
}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y) {
  for (int i = 0; i < 4; i++){
    float diffx = x - perspectiveVertices[i].pt.x-100;
    float diffy = y - perspectiveVertices[i].pt.y-100;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < perspectiveVertices[i].radius){
      perspectiveVertices[i].bOver = true;
    } else {
      perspectiveVertices[i].bOver = false;
    }	
  }
  multi.mouseMoved(x,y);
  frame1.mouseMoved(x,y);

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{
  gui.mouseDragged(x, y, button);
  if (mode_industrie){
    indus_new_pos.x = (x-indus_monitor.x)/indus_monitor_ratio;
    indus_new_pos.y = (y-indus_monitor.y)/indus_monitor_ratio;
  }

  for (int i = 0; i < 4; i++){
    if (perspectiveVertices[i].bBeingDragged == true){
      perspectiveVertices[i].pt.x = x - 100;
      perspectiveVertices[i].pt.y = y - 100;
    }
  }
  multi.mouseDragged(x, y, button);
  frame1.mouseDragged(x, y, button);
}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{
  gui.mousePressed(x, y, button);
  if (mode_industrie){
    indus_new_pos.x = (x-indus_monitor.x)/indus_monitor_ratio;
    indus_new_pos.y = (y-indus_monitor.y)/indus_monitor_ratio;
  }

  for (int i = 0; i < 4; i++){
    float diffx = x - perspectiveVertices[i].pt.x-100;
    float diffy = y - perspectiveVertices[i].pt.y-100;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < perspectiveVertices[i].radius){
      perspectiveVertices[i].bBeingDragged = true;
    } else {
      perspectiveVertices[i].bBeingDragged = false;
    }	
  }
  multi.mousePressed(x, y, button);
  frame1.mousePressed(x, y, button);
}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{
  gui.mouseReleased();
  for (int i = 0; i < 4; i++){
    perspectiveVertices[i].bBeingDragged = false;	
  }
  multi.mouseReleased(x, y, button);
  frame1.mouseReleased(x, y, button);
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{}

// --- Listen midi events -.----
void testApp::newMidiMessage(ofxMidiMessage &args){
  int 	port;
  int	channel;
  int 	status;
  //int 	control;
  //int 	value;
  double  timestamp;
  //logger.log(OF_LOG_NOTICE, "event - %s", logStr.c_str());

  stringstream midi_log;
  midi_log << args.channel
  	    << " status: " << args.status
  	    << " control: " << args.control
  	    << " value: " << args.value;
  midi_logger.log(OF_LOG_NOTICE, "event - %s", midi_log.str().c_str());
  

  
  switch(args.channel){
  case 5:
    switch(args.control){
      
    case 0:
      mode_zoom = args.value;
      gui.setValueB("MODE_ZOOM", mode_zoom);
      break;

    case 1: // "play"
      // mode industrie
      mode_industrie = args.value;
      gui.setValueB("MODE_INDUSTRIE", mode_industrie);
    break;

    case 2: // "forward"
      // mode_delay_nano
      mode_delay_nano = args.value;
      gui.setValueB("MODE_DELAY_NANO", mode_delay_nano);
      break;
      
    case 3: // "loop"
      mode_naked_stairs_draw = args.value;
      gui.setValueB("MODE_NAKED_STAIRS_DRAW", mode_naked_stairs_draw);
      mode_classic_draw = false;
      gui.setValueB("MODE_CLASSIC_DRAW", mode_classic_draw);
      init_ecran = true;
      break;

    case 4: // "stop"
      // mode normal draw STOP
      mode_classic_draw = args.value;
      gui.setValueB("MODE_CLASSIC_DRAW", mode_classic_draw);
      break;
    
    case 5: //  "record"
      // activation kinect
      mode_kinect = args.value;
      gui.setValueB("MODE_KINECT", mode_kinect);
      break;
      
      // END BLOCK LECTURE
      //-------------------------------------------------------//
      // BEGIN 8-9 CAPTURE

    case 27:
      threshold = args.value;
      break;

    case 28: // knob 8
      // reglage fondu noir 
      noir_fondu = args.value*2;
    break;
    case 11:
      seuil_son = ofMap(args.value, 0, 127, 0, 10);
      break;

    case 15: // fader 5
      screen_bis.x = ofMap(args.value, 0, 127, 0, 500);
      break;

    case 16: // fader 6
      screen_bis_ratio = ofMap(args.value, 0, 127, 0., 2.);
      screen_bis.y = (1-screen_bis_ratio)*screen_init.height;
      screen_bis.height = screen_init.height * screen_bis_ratio;
      screen_bis.width = screen_init.width * screen_bis_ratio;
      break;

    case 17: // fader 8
      // reglage seuil proche (8 fader)
      near_threshold = ofMap(args.value, 0, 127, 255, 0); // 255 - args.value*2;
      //sprintf(xml_name,"NEAR_THRESHOLD");
      //gui.setValueI(xml_name, near_threshold);
      //gui.saveSettings();
      break;
      
    case 18: // fader 9
      //reglage seuil lointain (9 fader)
      far_threshold = ofMap(args.value, 0, 127, 255, 0); //  255 - args.value*2
      break;
      
    case 106:  // but up 7
      // switch mode 20 correct   
      correction_20 = args.value;
      break;
      
    case 108: // but up 8
      // switch mode bgkinect   
      mode_bg_kinect = args.value;
      break;

    case 118: // but down 8
      // switch nouveau bg
      bLearnBakground = !bLearnBakground;
      break;
      
    case 107:// but up 9
      // switch mode profondeur / seuil (9 up)
      mode_depth = args.value;
      gui.setValueB("MODE_DEPTH", mode_depth);
      break;
      
    case 115:
      mode_masque_bord = args.value;
      break;

    case 116: // but down 9
      // switch mode live (9 down) 
      mode_live = args.value;
      gui.setValueB("MODE_LIVE", mode_live);
      break;
    } 
  break;

  case 3:
    switch(args.control){
    case 11:
      //indus_new_ratio[0] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 21:
      indus_size_factor[0] = ofMap(args.value, 0, 127, 0.1, 1);
      break;
    case 111: //
      indus_mode_live[0] = args.value;
      break;
    case 61:
      indusKnobButton(0);
      break;

    case 12:
      indus_new_ratio[1] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 22:
      indus_size_factor[1] = ofMap(args.value, 0, 127, 0.1, 1);
      break; 
    case 112: //
      indus_mode_live[1] = args.value;
      break;
    case 62:
      indusKnobButton(1);
      break;

    case 13:
      indus_new_ratio[2] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 23:
      indus_size_factor[2] = ofMap(args.value, 0, 127, 0.1, 1);
      break; 
    case 113: //
      indus_mode_live[2] = args.value;
      break;
    case 63:
      indusKnobButton(2);
      break;

    case 14:
      indus_new_ratio[3] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 24:
      indus_size_factor[3] = ofMap(args.value, 0, 127, 0.1, 1);
      break; 
    case 114: //
      indus_mode_live[3] = args.value;
      break;
    case 64:
      indusKnobButton(3);
      break;

    case 15:
      indus_new_ratio[4] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 25:
      indus_size_factor[4] = ofMap(args.value, 0, 127, 0.1, 1);
      break; 
    case 115: //
      indus_mode_live[4] = args.value;
      break;
    case 65:
      indusKnobButton(4);
      break;

    case 16:
      indus_new_ratio[5] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 26:
      indus_size_factor[5] = ofMap(args.value, 0, 127, 0.1, 1);
      break; 
    case 116: //
      indus_mode_live[5] = args.value;
      break;
    case 66:
      indusKnobButton(5);
      break;
      
    case 17:
      indus_new_ratio[6] = ofMap(args.value, 0, 127, 0.05, 3);
      break;
    case 27:
      indus_size_factor[6] = ofMap(args.value, 0, 127, 0.1, 1);
      break; 
    case 117: //
      indus_mode_live[6] = args.value;
      break;
    case 67:
      indusKnobButton(6);
      break;
 
    case 18:
      for (int i = 0; i<n_vids; i++){
	indus_recorder[i].setLatence(ofMap(args.value, 0, 127, 0, 10));
      }
      break;
 
    case 28:
      indus_new_angle = ofMap(args.value, 0, 127, -180, 180);
      break;

    case 108:
      indus_hold = args.value;
      break;

    case 119:
      for (int i = 0; i<n_vids; i++){
	indus_set_frame[i] = true;
	indus_set_frame_pos[i] = 0;
      }
      break;

    case 109:
      if (args.value == 0){
	for (int i = 0; i<n_vids; i++){
	  indus_recorder[i].stop();
	}	
      }else{
	for (int i = 0; i<n_vids; i++){
	  indus_recorder[i].play();
	}
      }
      
      break;
    }

  // END 8-9 CAPTURE control
  //--------------------------------------------------------------------//
  // BEGIN REGLAGE SORTIE scene 3
   
  // case 3:
  //   switch(args.control){

  //   case 11:
  //     screen_rect.x = ofMap(args.value, 0, 127, 0, 500);
  //     break;

  //   case 12: // fader 5
  //     screen_ratio = ofMap(args.value, 0, 127, 0., 2.);
  //     screen_rect.y = (1-screen_ratio)*screen_init.height;
  //     break;


  //   // case 12:
  //   //   screen_rect.y = ofMap(args.value, 0, 127, 0, 500);
  //   //   screen_ratio = (screen_rect.height_init - screen_rect.y)/screen_rect.height_init;
  //   //   break;

  //   case 101:
  //     init_ecran = true;
  //     break;

  //   case 111:
  //     draw_point_screen = !draw_point_screen;
  //     break;

  //   case 102:
  //     save_screen = true;
  //     screen_bak = screen_rect;
  //     screen_ratio_bak = screen_ratio;
  //     crop_left_bak = crop_left;
  //     crop_right_bak = crop_right;
  //     crop_up_bak = crop_up;
  //     break;
      
  //   case 112:
  //     bak_screen = true;
  //     break;
      
  //   case 21: // potar 1.3
  //     crop_left = args.value * screen_rect.width / 255;
  //     break;
      
  //   case 22: // potar 2.3
  //     crop_right = args.value * screen_rect.width / 255;
  //     break;
      
  //   // case 11: // fadder 1.3
  //   //   crop_up = args.value * screen_rect.height / 255;
  //   //   break;
      
  //   case 103: // but up 3
  //     // switch perspective
  //     if (args.value == 127){
  // 	mode_warp_perspective = !mode_warp_perspective;
  //     }
  //     break;

  //   case 104: // but up 4
  //     // maintenir le bouton appuyer pour reglage vertical
  //     set_vertical = !set_vertical;
  //     break;

  //   case 23: // potar 3.3
  //     // set up left corner
  //     if (set_vertical){
  // 	up_left_corner.set(up_left_corner.x,-(63 - args.value)*input_width/127);
  //     }else{
  // 	up_left_corner.set((63 - args.value)*input_width/127,up_left_corner.y);
  //     }
  //     break;
      
  //   case 13: // fader 3.3
  //     // set down left corner
  //     if (set_vertical){
  // 	down_left_corner.set(down_left_corner.x,input_height-(63 - args.value)*input_height/127);
  //     }else{
  // 	down_left_corner.set(-(63 - args.value)*input_width/127,down_left_corner.y);
  //     }
  //     break;
      
  //   case 24: // potar 4
  //     // set up right corner
  //     if (set_vertical){
  // 	up_right_corner.set(up_right_corner.x,-(63-args.value)*input_height/127);
  //     }else{
  // 	up_right_corner.set(input_width+(63-args.value)*input_width/127,up_right_corner.y);
  //     }
  //     break;
      
  //   case 14: // fader 4
  //     // set down right corner
  //     if (set_vertical){
  // 	down_right_corner.set(down_right_corner.x, input_height-(63-args.value)*input_height/127);
  //     }else{
  // 	down_right_corner.set(input_width+(63-args.value)*input_width/127,down_right_corner.y);
  //     }
  //     break;

  //   }
  //   break;

  // END REGLAGE SORTIE
  //-------------------------------------------------------//
  // BEGIN SCENE 1

  case 1:
    switch(args.control){      
      //------------//
      // FLASH 1.1
      
    case 101: // but up 1.1
      // declenchement flash
      mode_flash = args.value;
      break;
      
    case 111: // but up 1.1
      // declenchement flash
      bool_flash = true;
      break;
      
    case 11: // fader 2.1
      // delay flash
      delay = ofMap(args.value, 0, 127, 0, 25);
      break;
      

    case 61:
      mode_flash_impulse = args.value;
      break;
      
      //------------//
      // ESCALIER 2.1
      

      //case 11:
      //nakedStairs.setTransition(0, ofMap(args.value, 0, 127, 0, 200));
      //break;
    case 12:
      nakedStairs.setTransition(1, ofMap(args.value, 0, 127, 0, 200));
      break;
    case 13:
      nakedStairs.setTransition(2, ofMap(args.value, 0, 127, 0, 200));
      break;
    case 14:
      nakedStairs.setTransition(3, ofMap(args.value, 0, 127, 0, 200));
      break;
    case 15:
      nakedStairs.setTransition(4, ofMap(args.value, 0, 127, 0, 200));
      break;
    case 16:
      nakedStairs.setTransition(5, ofMap(args.value, 0, 127, 0, 200));
      break;

    case 22: // knob 2
      nakedStairs.setPosX(ofMap(args.value, 0, 127, 0, 200));
      break;
    case 102: // but up 2
      nakedStairs.increaseDelay();
      break;
    case 112: // but down 2
      nakedStairs.decreaseDelay();
      break;      

    case 104: 
      nakedStairs.increaseNsnum();
      break;
    case 114:
      nakedStairs.decreaseNsnum();
      break;
    case 103:
      ns_draw_point_screen = !ns_draw_point_screen;
      break;
	
    case 113:
      ns_corner_from_bak=true;
      break;
    case 23:
      ns_screen.x = ofMap(args.value, 0, 127, 0, 500);
	break;
    case 24:
      ns_screen.y = ofMap(args.value, 0, 127, 0, 500);
           
      //------------//
      // LOOP 3.1 4.1
      
    // case 103: // but up 3
    //   loops[0].setLoop(recorder.getCurrentFrame());
    //   break;
      
    // case 113: // but down 3
    //   loops[0].setPlayMode();
    //   break;
      
    // case 104: // but up 4
    //   loops[1].setLoop(recorder.getCurrentFrame());
    //   break;

    // case 114: // but down 4
    // 	loops[1].setPlayMode();
    //   break;
      
    }
  break;

  // END SCENE 1
  //-------------------------------------------------------//
  
  case 10:
    if (args.control == 10){
      impulse_value = args.value;
    }
    impulse = true;
    break;

  case 2:
    switch(args.control){

      //// MODES /////
    case 21:
      delay_decrease_value = ofMap(args.value, 0, 127, 0.2, 1);
      break;
    case 101: // but up 1
      mode_delay_fixe = args.value;
      gui.setValueB("MODE_DELAY_FIXE", mode_delay_fixe);
      break;
    case 111:
      mode_delay_gray = args.value;
      gui.setValueB("MODE_DELAY_GRAY", mode_delay_gray);
      break;
    case 61:
      mode_delay_impulse = args.value;
      gui.setValueB("MODE_DELAY_IMPULSE", mode_delay_impulse);
      break;
    case 62:
      mode_delay_end = args.value;
      break;

      //// Values ////
    case 22: // number of delays
      n_delay = ofMap(args.value, 0, 127, 0, 20);
      break;
    case 102:
      n_delay ++;
      break;
    case 112:
      n_delay --;
      if (n_delay < 0){
	n_delay = 0;
      }
      break;

    case 23: // time between delays
      delay_lenght = ofMap(args.value, 0, 127, 0, 60) ;
      break;
    case 103:
      delay_lenght ++;
      break;
    case 113:
      delay_lenght --;
      if (delay_lenght<0){
	delay_lenght = 0;
      }
      break;

      //// Tap tempo /////
    case 24:
      delay_offset = ofMap(args.value, 0, 127, 0 ,5);
      if (delay_offset > delay_lenght){
	delay_offset = delay_lenght;
      }
      break;

    case 104:
      delay_learn_tap = args.value;
      if (delay_learn_tap){
	delay_tap_n = 0;
      }
      break;

      
    // CONTROLE DELAY NANO      
    case 11: // fader 1.2
      compteur_delay[0] = ofMap(args.value, 0, 127, 0, 30);
      break;
    case 12: // fader 2.2
      compteur_delay[1] = ofMap(args.value, 0, 127, 0, 60);
      break;
    case 13: // fader 3.2
      compteur_delay[2] = ofMap(args.value, 0, 127, 0, 120);
      break;
    case 14: // fader 4.2
      compteur_delay[3] = ofMap(args.value, 0, 127, 0, 180);
      break;
    case 15: // fader 5.2
      compteur_delay[4] = ofMap(args.value, 0, 127, 0, 240);  
      break;     
    }
    break;
    
  case 4:
    // CONTROL INDUS
    switch(args.control){	
    case 1:
      indus_mode_size = args.value;
      gui.setValueB("INDUS_MODE_SIZE", indus_mode_size);
      break;
    case 2:
      indus_mode_rond = args.value;
      gui.setValueB("INDUS_MODE_ROND", indus_mode_rond);
      break;
    case 3:
      // indus_mode_grid = args.value;
      // Indus_mode_grid_switch = true;
      break;

    case 21:
      //indus_new_ratio[0] = ofMap(args.value, 0, 127, 0.2, 3);
      indus_angle[0] = ofMap(args.value, 0, 127, -180, 180);  
      break;

    case 11:
      indus_new_ratio[0] = ofMap(args.value, 0, 127, 0.2, 3);
      //indusKnob(0, args.value);
      break;

    case 101: //
      if (!indus_hold){
	if (!indus_recorder[0].getRecordMode()){
	  indus_recorder[0].setRecordModeOn();
	  indus_start_master_loop = ofGetElapsedTimeMillis();
	}else{
	  indus_recorder[0].setRecordModeOff();
	  indus_recorder[0].play();
	  if (indus_sync_loop){
	    
	    indus_sync_lenght = indus_recorder[0].getLoopLenght();
	    indus_sync_lenght_ms = indus_recorder[0].getLoopLenghtMillis();
	    //indus_recorder[0].play();
	  } 
	  
	}
      }
      break;
    case 111: //
      indus_mode_live[0] = args.value;
      break;
    case 61:
      indusKnobButton(0);
      break;
    case 71:
      indusStepUp(0);
      break;
    case 81:
      indusStepDown(0);
      break;

    case 22:
      indus_angle[1] = ofMap(args.value, 0, 127, -180, 180);  
      break;
    case 12:
      indusKnob(1, args.value);
      break;
    case 102: // 
      indusSetRecord(1);
      break;
    case 112: //
      indus_mode_live[1] = args.value;
      break;
    case 62:
      indusKnobButton(1);
      break;
    case 72:
      indusStepUp(1);
      break;
    case 82:
      indusStepDown(1);
      break;

    case 23:
      indus_angle[2] = ofMap(args.value, 0, 127, -180, 180);  
      break;
    case 13:
      indusKnob(2, args.value);
      break;
    case 103: // 
      indusSetRecord(2);
      break;
    case 113: //
      indus_mode_live[2] = args.value;
      break;
    case 63:
      indusKnobButton(2);
      break;
    case 73:
      indusStepUp(2);
      break;
    case 83:
      indusStepDown(2);
      break;

    case 24:
      indus_angle[3] = ofMap(args.value, 0, 127, -180, 180);  
      break;
    case 14:
      indusKnob(3, args.value);
      break;
    case 104: // 
      indusSetRecord(3);
      break;
    case 114: //
      indus_mode_live[3] = args.value;
      break;
    case 64:
      indusKnobButton(3);
      break;
    case 74:
      indusStepUp(3);
      break;    
    case 84:
      indusStepDown(3);
      break;

    case 25:
      indus_angle[4] = ofMap(args.value, 0, 127, -180, 180);  
      break;
    case 15:
      indusKnob(4, args.value);
      break;
    case 105: // 
      indusSetRecord(4);
      break;
    case 115: //
      indus_mode_live[4] = args.value;
      break;
    case 65:
      indusKnobButton(4);
      break;
    case 75:
      indusStepUp(4);
      break;    
    case 85:
      indusStepDown(4);
      break;

    case 26:
      indus_angle[5] = ofMap(args.value, 0, 127, -180, 180);  
      break;
    case 16:
      indusKnob(5, args.value);
      break;
    case 106: // 
      indusSetRecord(5);
      break;
    case 116: //
      indus_mode_live[5] = args.value;
      break;
    case 66:
      indusKnobButton(5);
      break;
    case 76:
      indusStepUp(5);
      break;    
    case 86:
      indusStepDown(5);
      break;
      
    case 27:
      indus_angle[6] = ofMap(args.value, 0, 127, -180, 180);  
      break;
    case 17:
      indusKnob(6, args.value);
      break;
    case 107: // 
      indusSetRecord(6);
      break;
    case 117: //
      indus_mode_live[6] = args.value;
      break;
    case 67:
      indusKnobButton(6);
      break;
    case 77:
      indusStepUp(6);
      break;
    case 87:
      indusStepDown(6);
      break;

    case 18:
      for (int i = 0; i<n_vids; i++){
	indus_recorder[i].setLatence(ofMap(args.value, 0, 127, 0, 10));
      }
      break;
    // case 28:
    //   indus_new_ratio[7] = ofMap(args.value, 0, 127, 0.2, 3);
    //   break;
    // case 18:
    //   //indus_set_frame[7] = true;
    //   //indus_set_frame_pos[7] = ofMap(args.value, 0, 127, 0, 1);
    //   break;
    // case 108: // 
    //   indusSetRecord(7);
    //   break;
    // case 118: //
    //   indus_recorder[7].setPlayMode();
    //   indus_set_pause[7] = true;
    //   break;

    case 28:
      indus_new_angle = ofMap(args.value, 0, 127, -180, 180);
      break;

    case 108:
      indus_hold = args.value;
      break;

    case 119:
      for (int i = 0; i<n_vids; i++){
	indus_set_frame[i] = true;
	indus_set_frame_pos[i] = 0;
      }
      break;

    case 109:
 
      if (args.value == 0){
	for (int i = 0; i<n_vids; i++){
	  indus_recorder[i].stop();
	}	
      }else{
	for (int i = 0; i<n_vids; i++){
	  indus_recorder[i].play();
	}
      }
      
      break;
    }

    break;
  
  }
  
  cout << "MIDI message [channel: " << args.channel << ", status: " << args.status << ", control: " << args.control << ", value: " << args.value << "]" << endl;
}

void testApp::indusButton(int i){
  
}

void testApp::indusKnob(int i, int val){
  //if (indus_mode_size){
    indus_new_ratio[i] = ofMap(val, 0, 127, 0, 3);
    //}else{
    //    indus_set_frame[i] = true;
    //    indus_set_frame_pos[i] = ofMap(val, 0, 127, 0, 1);
    //}
}

void testApp::indusKnobButton(int i){
      if (indus_learn_size[i] == -1){
	indus_learn_size[i] = 1;
      } else if (indus_learn_size[i] == 0){
	indus_learn_size[i] = 1;
      } else if (indus_learn_size[i] == 1){
	indus_learn_size[i] = 0;
      }
}

void testApp::indusStepUp(int i){
  for (int z = 0; z < n_vids-1; z++){
    cout << i << endl;
    if (indus_layer[z] == i){
      
      int i_bak = indus_layer[z+1];      
      cout << "i " << i << " z " << z << " i_bak " << i_bak <<endl;
      
      indus_layer[z+1] = i;
      indus_layer[z] = i_bak;
      cout << "z+1 " << z+1 << "lay " << indus_layer[z+1] << " z " << z << " lay " << indus_layer[z] << endl;
      cout << i_bak <<endl;
      break;
    }
    
  }
}

void testApp::indusStepDown(int i){
  cout << "bef stepDown "; 
  for (int j = 0; j < n_vids; j++){
    cout << indus_layer[j] << " " ;
  }
  cout << endl;
  for (int z = 1; z < n_vids; z++){
    if (indus_layer[z] == i){
      int i_bak = indus_layer[z-1];
      indus_layer[z-1] = i;
      indus_layer[z] = i_bak;
    }
  }
  cout << "aft stepDown "; 
  for (int j= 0; j < n_vids; j++){
    cout << indus_layer[j] << " " ;
  }
  cout << endl;
}

void testApp::indusSetRecord(int i){
  int value;
  value = 10 +i;
  if (!indus_hold){
    if (!indus_recorder[i].getRecordMode()){
      midiOut.sendNoteOn(7, value, 127);
      indus_recorder[i].setRecordModeOn();
    }else{
      //if (!indus_sync_loop){
      //indus_recorder[i].s.etRecordModeOff();
    //}else{
      if (indus_sync_loop){
	midiOut.sendNoteOff(7, value, 0);
	indus_sync_set_stop[i] = true;
      }else{
	indus_recorder[i].setRecordModeOff();
	indus_recorder[i].play();
      }
      
    }
  }
}

//--------------------------------------------------------------
void testApp::exit() {
  if (mode_kinect){
    kinect.setCameraTiltAngle(0); // zero the tilt on exit
    kinect.close();
  }
}

void testApp::audioReceived (float * input, int bufferSize, int nChannels){

//   float tot = 0;
//   //cout << "process audio " << endl;
//   // 	// samples are "interleaved"
//   if (ofGetElapsedTimeMillis() > 1000){
//     for (int i = 0; i < bufferSize; i++){
//       left[i] = input[i*2];
//       tot += left[i];
//       right[i] = input[i*2+1];
//     }
//     //cout << tot;
//     son_val = tot;

//     if (tot > 1){
//       impulse =true;
//     }

//     if (son_val < 0){
//       son_val=0;
//     }
//     liste_amp.pop();    
//     liste_amp.push(son_val);
//     // AA.processAudio(left, bufferSize);
//   }
//   // ////    for (int i = 1; i < buff_size-1; i++){
//   // ////        liste[i] = liste[i-1];
//   // ////    }
//   // ////
//   // ////    liste[0] = AA.pitch;
//   // ////    mean_pitch = 0;
//   // ////
//   // ////    for (int i; i < buff_size-1; i++){
  //core.audioReceived(input, bufferSize, nChannels);  
}

void testApp::audioRequested(float * output, int bufferSize, int nChannels){
  //core.audioRequested(output, bufferSize, nChannels);
}

