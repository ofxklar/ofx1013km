#ifndef DELAY_PARTICLES_H
#define DELAY_PARTICLES_H

#pragma once

#include "simpleParticle.h"
#include "utils.h"

class DelayParticles
{
 public:
  DelayParticles();
  virtual ~DelayParticles();

  void update();
  void draw();

  void addParticle(ofPoint pos, ofPoint vit, int size, int col);
 protected:
  std::vector<SimpleParticle> particleList;
};

#endif
