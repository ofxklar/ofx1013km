#include "ofMain.h"
#include "guiCustom.h"

void drawFadder(int x, int y, int w, int val){
  ofSetColor(255,255,255);
  ofLine(x, y, x + w, y);
  
  ofSetColor(255,0,0);
  ofCircle(x+ val*w/255, y , 5);
  
  stringstream reportStream;
  reportStream << " " << val << endl;
  ofDrawBitmapString(reportStream.str(), x, y + 15);
  ofSetColor(255,255,255);
}

void drawFadderVert(int x, int y, int h, int val){
  ofSetColor(255,255,255);
  ofLine(x, y, x , y+h);
  
  ofSetColor(255,0,0);
  ofCircle(x, y + val*h/255, 5);
  
  stringstream reportStream;
  reportStream << " " << val << endl;
  ofDrawBitmapString(reportStream.str(), x, y );
  ofSetColor(255,255,255);
}

void drawSwitch(int x, int y, bool mode){
  int ww = 20;
  int hh = 20;
  if (mode){
    ofSetColor(0,255,0);
  }else{
    ofSetColor(255,0,0);
  }
  ofRect(x, y, ww, hh);
  ofSetColor(255,255,255);
}
