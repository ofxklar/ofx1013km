#include "k2mNote.h"

K2mNote::K2mNote()
{
}

void K2mNote::init(ofxMidiOut & midi_out_in, int channel_in, int note_in, ofRectangle key_in){
  midi_out = midi_out_in;
  channel = channel_in;
  note = note_in;
  key = key_in;
  send = false;
  velocity = 0;
  lock = false;
}

void K2mNote::update(int x, int y, int velocity_in){
  if (!lock){
    bool isOver = isInKey(x, y);
    lock = isOver;
    if (isOver and (!send or velocity_in > velocity)){
      sendNoteOn(velocity_in);
    }else if(!isOver and send){
      sendNoteOff();
    }
  }
}

bool K2mNote::isInKey(int x, int y){
  if ((key.x < x) and (x < key.x + key.width)){ 
    if ((key.y < y) and (y < key.y + key.height)){
      return true;
    }
  }
  return false;
}

void K2mNote::unLock(){
  lock = false;
}

void K2mNote::sendNoteOn(int velocity_in){
  midi_out.sendNoteOn(channel, note, velocity_in);
  send = true;
  velocity = velocity_in;
}

void K2mNote::sendNoteOff(){
  if (send){
    midi_out.sendNoteOff(channel, note, 0);
    send = false;
    velocity = 0;
  }
}

void K2mNote::draw(){
  ofFill();
  if (send){
    ofSetColor(0,0,255,velocity*2);
    ofRect(key.x, key.y, key.width, key.height);
  }
  ofSetColor(255,255,255, 255);
  ofNoFill();
  ofRect(key.x, key.y, key.width, key.height);
}

K2mNote::~K2mNote()
{
}
