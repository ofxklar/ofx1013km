#include "vidDelay.h"

VidDelay::VidDelay()
{
}

void VidDelay::init(LoopRecorder & recorder){
  recorder = recorder;
  width = recorder.width;
  height = recorder.height;
  buffer_lenght = recorder.loop_lenght;
  pixel_type = recorder.pixel_type;

  n_delay = 0;
  delay_lenght = 0;
  delay_tap = 0;
  delay_offset = 0;
  delay_decrease_value = 1;
  mode_delay_fixe = false;
  mode_delay_nano = false;
  mode_delay_gray = false;
  mode_delay_end = false;
  mode_delay_impulse = false;

  sortie.allocate(width, height, OF_IMAGE_GRAYSCALE);
  for (int i = 0; i < DELAYMAX; i++){
    i_delay[i] = 0;
  }
}

void VidDelay::update(bool impulse){
  
  if (mode_delay_impulse){
    if (impulse){
      n_delay ++;
      n_delay = min(n_delay, 20);
      impulse = false;
    }else{
      n_delay = 0;
    }
  }

  // tap tempo delay fixe
  if (delay_learn_tap){
    delay_tap_n += 1;
    cout << "delay_learn_tap tap_n " << delay_tap_n << endl;
  }
  if (delay_tap_n >= 0 and !delay_learn_tap){
    delay_tap = delay_tap_n;
    delay_tap_n = -1;
    cout << "tap_n > 0;; delay_tap " << delay_tap << " tap_n " << delay_tap_n << endl;
  }
  
  // delay fixe
  if (mode_delay_fixe && !mode_delay_end){
    if (delay_tap !=0){
      delay_lenght = delay_tap;
      delay_tap = 0;
      cout << "mode_delay_fixe " << delay_tap << " " << delay_lenght << endl;
    }
    for (int i=0; i<n_delay; i++){
      i_delay[i] = i * delay_lenght - delay_offset;
    }
  }
  
  // delay fin
  if (mode_delay_end){
    for (int i= 1 ; i<n_delay; i++){
      if (i_delay[i] > 0){
	i_delay[i] --;
      }
    }
  } 

  int delay0 = 1;
  if (mode_live){
    delay0 = 0;
  }

  for (int i = delay0; i < n_delay; i++){
    if (i_delay[i]>0){
      int n_frame_delay = recorder.getCurrentFrameRecord() - i_delay[i];
      while (n_frame_delay <= 0){
	n_frame_delay += buffer_lenght;
      }
      
      ofxCvGrayscaleImage image_delay = recorder.getFrameN(n_frame_delay-1);
      if (mode_delay_gray){
	image_delay -= ofMap(i, 0, n_delay, 0, delay_decrease_value * 255);
      }
      //addImage(sortie, image_delay, 0);
    }
  } 
}

void VidDelay::drawMonitor(int x, int y, int width){
  ofPushMatrix();
  ofTranslate(x, y);

    ofSetColor(100, 100, 100);
    ofRect(0, 50, width, 15);
    ofSetColor(255, 255, 255);
    
    int nn;
    if (mode_delay_nano){
      nn = 5;
    } else if (mode_delay_fixe){
      nn = n_delay;
    } else {
      nn=0;
    }
    for (int i = 0; i < nn;i++){
      if (i_delay[i]!=0){
	int pos_x = ofMap(i_delay[i], 0, 300, 0, width);
	ofLine(pos_x, 48, pos_x, 67);
	stringstream no_vid;
	no_vid << i << endl
	       << i_delay[i];
	ofDrawBitmapString(no_vid.str(), pos_x, 65);
      }
    }
    ofPopMatrix();

}

VidDelay::~VidDelay()
{
}
