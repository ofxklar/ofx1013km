#include "screenFrameTex.h"

ScreenFrameTex::ScreenFrameTex(){
  for (int i = 0; i < 4; i++){
    perspectiveVertices[i].bOver = false;
    perspectiveVertices[i].bBeingDragged = false;
    perspectiveVertices[i].radius = 7;
  }
  centerPoint.bOver = false;
  centerPoint.bBeingDragged = false;
  centerPoint.radius = 10;
  anchorPercent.set(0.5, 1);
  centerPoint.pt.set(0, 0);

  pos_x = 0;
  pos_y = 0;
  ratio_x = 1.0;
  ratio_y = 1.0;
  angle = 0;
  angle_bak = 0;

}

void ScreenFrameTex::setImage(ofxCvGrayscaleImage & image_in, ofBlendMode blend_mode){
  width = image_in.width;
  height = image_in.height;
  int w = image_in.width;
  int h = image_in.height;
  center.set(w/2, h/2);
  
  blend_mode = blend_mode;

  image_out.allocate(w, h, GL_RGBA);
  image_out.setAnchorPercent(anchorPercent.x, anchorPercent.y);

  //image_out = image_in;

  warp_in[0].set(0, 0);
  warp_in[1].set(w, 0);
  warp_in[2].set(w, h);
  warp_in[3].set(0, h);

  warp_out[0].set(0, 0);
  warp_out[1].set(w, 0);
  warp_out[2].set(w, h);
  warp_out[3].set(0, h);

  perspectiveVertices[0].pt.set(0,0);
  perspectiveVertices[1].pt.set(w,0);
  perspectiveVertices[2].pt.set(w,h);
  perspectiveVertices[3].pt.set(0,h);
}

void ScreenFrameTex::setImage(ofImage & image_in, ofBlendMode blend_mode){
  width = image_in.width;
  height = image_in.height;
  int w = image_in.width;
  int h = image_in.height;
  center.set(w/2, h/2);
  centerPoint.pt.set(0, 0);
  
  blend_mode = blend_mode;

  image_out.allocate(w, h, GL_RGBA);
  image_out.setAnchorPercent(anchorPercent.x, anchorPercent.y);

  pos_x = 0;
  pos_y = 0;
  ratio_x = 1;
  ratio_y = 1;
  angle = 0;
  angle_bak = 0;

  warp_in[0].set(0, 0);
  warp_in[1].set(w, 0);
  warp_in[2].set(w, h);
  warp_in[3].set(0, h);

  warp_out[0].set(0, 0);
  warp_out[1].set(w, 0);
  warp_out[2].set(w, h);
  warp_out[3].set(0, h);

  perspectiveVertices[0].pt.set(0,0);
  perspectiveVertices[1].pt.set(w,0);
  perspectiveVertices[2].pt.set(w,h);
  perspectiveVertices[3].pt.set(0,h);
}

void ScreenFrameTex::setAnchorPercent(int x_anchor, int y_anchor){
  anchorPercent.set(x_anchor, y_anchor);
  image_out.setAnchorPercent(x_anchor, y_anchor);
}

void ScreenFrameTex::setPos(ofPoint pos){
  pos_x = pos.x;
  pos_y = pos.y;
  centerPoint.pt = pos;
}

void ScreenFrameTex::setPos(int x, int y){
  pos_x = x;
  pos_y = y;
  centerPoint.pt.x = x;
  centerPoint.pt.y = y;
}

void ScreenFrameTex::setSize(int w, int h){
  ratio_x = (float)w/width;
  ratio_y = (float)h/height;
}

void ScreenFrameTex::setRatio(float ratio){
  ratio_x = ratio;
  ratio_y = ratio;
}

void ScreenFrameTex::setAngle(int angle_in){
  angle = angle_in;
}

void ScreenFrameTex::setWarpOut(ofPoint point_in[4]){
  for (int i = 0; i<4;i++){
    cout << "point_in " << point_in[i].y << endl;
    perspectiveVertices[i].pt = point_in[i];
  }
}

void ScreenFrameTex::initWarp(){
  perspectiveVertices[0].pt.set(0,0);
  perspectiveVertices[1].pt.set(width,0);
  perspectiveVertices[2].pt.set(width,height);
  perspectiveVertices[3].pt.set(0,height);
}

ofPoint ScreenFrameTex::getPos(){
  ofPoint point_out;
  point_out.set(pos_x, pos_y);
  return point_out;
}

float ScreenFrameTex::getRatio(){
  return ratio_x;
}

int ScreenFrameTex::getAngle(){
  return angle;
}

ofPoint ScreenFrameTex::getWarpPoint(int i){
  return perspectiveVertices[i].pt;
}

void ScreenFrameTex::update(ofxCvGrayscaleImage & image){
  warp_out[0] = perspectiveVertices[0].pt;
  warp_out[1] = perspectiveVertices[1].pt;
  warp_out[2] = perspectiveVertices[2].pt;
  warp_out[3] = perspectiveVertices[3].pt;
  
  //centerPoint.pt.x
  
  pos_x = centerPoint.pt.x;
  pos_y = centerPoint.pt.y;
  
  unsigned char *pixels_image = image.getPixels();
  unsigned char *pixels2;
  pixels2 = new unsigned char [height*width*4];
  for (int i = 0; i < height*width; i++){
    pixels2[i*4] = pixels_image[i];
    pixels2[i*4+1] = pixels_image[i];
    pixels2[i*4+2] = pixels_image[i];
    pixels2[i*4+3] = pixels_image[i];
  }
  
  findHomography(warp_in, warp_out, matrix);

  image_out.loadData(pixels2, width, height, GL_RGBA);
  free(pixels2);
  
  
  //image_out.setFromPixels(image.getPixels(), width, height);
  //image_out.warpIntoMe(image, warp_in, warp_out);
}

void ScreenFrameTex::drawFrame(){
  ofSetColor(0,0,255);
  if (centerPoint.bOver == true) ofFill();
  else ofNoFill();
  ofCircle(centerPoint.pt.x, centerPoint.pt.y,centerPoint.radius);

  ofPushMatrix();

    ofTranslate(pos_x , pos_y);
    ofRotate(angle);
    ofTranslate(- anchorPercent.x * width * ratio_x,  - anchorPercent.y * height * ratio_y);
    ofScale(ratio_x, ratio_y);
    ofNoFill();
    ofSetColor(255,0,0    );
    ofBeginShape();
    for (int j = 0; j<4; j++){
      ofVertex(perspectiveVertices[j].pt.x, perspectiveVertices[j].pt.y);
      if (perspectiveVertices[j].bOver == true) ofFill();
      else ofNoFill();
      ofCircle(perspectiveVertices[j].pt.x, perspectiveVertices[j].pt.y,perspectiveVertices[j].radius/ratio_y);
      ofNoFill();
    }
    ofEndShape(true);
    ofSetColor(255,255,255);
  ofPopMatrix();
}

//--------------------------------------------------------------
void ScreenFrameTex::drawImage(){
  ofPushMatrix();
  ofTranslate(pos_x, pos_y);     
  ofRotate(angle);
  ofScale(ratio_x, ratio_y);
  glMultMatrixf(matrix);
 //ofEnableBlendMode(blend_mode);
  image_out.draw(0,0);
  //ofDisableBlendMode();
  ofPopMatrix();
}

//--------------------------------------------------------------
void ScreenFrameTex::changeReference(int & x, int & y) {
  int x_bis = (x - pos_x)/ratio_x;
  int y_bis = (y - pos_y)/ratio_y;

  point1.set(x_bis, y_bis);

  x = x_bis*cos(angle*3.14/180)  + y_bis*sin(angle*3.14/180) + anchorPercent.x * width;
  y = -x_bis*sin(angle*3.14/180) + y_bis*cos(angle*3.14/180) + anchorPercent.y * height;
  point0.set(x, y);
}

//--------------------------------------------------------------
void ScreenFrameTex::mouseMoved(int x, int y) {

  float diffx = x - centerPoint.pt.x;
  float diffy = y - centerPoint.pt.y;
  float dist = sqrt(diffx*diffx + diffy*diffy);
  if (dist < centerPoint.radius){
    centerPoint.bOver = true;
  } else {
    centerPoint.bOver = false;
  }	

  changeReference(x, y);
  for (int i = 0; i < 4; i++){
    float diffx = x - perspectiveVertices[i].pt.x;
    float diffy = y - perspectiveVertices[i].pt.y;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < perspectiveVertices[i].radius/ratio_x){
      perspectiveVertices[i].bOver = true;
    } else {
      perspectiveVertices[i].bOver = false;
    }	
  }
}

//--------------------------------------------------------------
void ScreenFrameTex::mouseDragged(int x, int y, int button){
  if (centerPoint.bBeingDragged == true){
    if (button == 0){
      centerPoint.pt.x = x;
      centerPoint.pt.y = y;
    } else if (button == 1){
      setAngle(angle_bak + (x - x_bak)/3);
    }
  }


  changeReference(x, y);
  for (int i = 0; i < 4; i++){
    if (perspectiveVertices[i].bBeingDragged == true){
      perspectiveVertices[i].pt.x = x;
      perspectiveVertices[i].pt.y = y;
    }
  }
}

//--------------------------------------------------------------
void ScreenFrameTex::mousePressed(int x, int y, int button){
  x_bak = x;
  y_bak = y;
  angle_bak = angle;

  float diffx = x - centerPoint.pt.x;
  float diffy = y - centerPoint.pt.y;
  float dist = sqrt(diffx*diffx + diffy*diffy);
  if (dist < centerPoint.radius){
    centerPoint.bBeingDragged = true;
    if (button == 3){
      if ((ratio_x == 0)or (ratio_y == 0)){
	ratio_x = 0.05;
	ratio_y = 0.05;
      }
      ratio_x *= 1.05;
      ratio_y *= 1.05;
    }else if(button == 4){
      ratio_x *= 0.95;
      ratio_y *= 0.95;
    }
  } else {
    centerPoint.bBeingDragged = false;
  }	

  changeReference(x, y);
  for (int i = 0; i < 4; i++){
    float diffx = x - perspectiveVertices[i].pt.x;
    float diffy = y - perspectiveVertices[i].pt.y;
    float dist = sqrt(diffx*diffx + diffy*diffy);
    if (dist < perspectiveVertices[i].radius/ratio_x){
      perspectiveVertices[i].bBeingDragged = true;
    } else {
      perspectiveVertices[i].bBeingDragged = false;
    }	
  }
}

//--------------------------------------------------------------
void ScreenFrameTex::mouseReleased(int x, int y, int button){
  for (int i = 0; i < 4; i++){
    perspectiveVertices[i].bBeingDragged = false;	
  }
  centerPoint.bBeingDragged = false;	
}
//--------------------------------------------------------------
ScreenFrameTex::~ScreenFrameTex()
{
    //dtor
}
