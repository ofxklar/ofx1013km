#include "multiFrame.h"


MultiFrame::MultiFrame(){
  n_screen_frames = 0;
  anchorPercent.set(0.5, 1);
}

void MultiFrame::init(int n_frames){
  n_screen_frames = n_frames;
  if( XML.loadFile("multiFrameBak.xml") ){
    cout << "multiFrameBak.xml loaded!" << endl;
  }else{
    cout << "unable to load multiFrameBak.xml check data/ folder" << endl;
  }
  
  numFrameTag = XML.getNumTags("SCREENFRAME");
  cout << " numTag " << numFrameTag << endl;

  //loadXmlBak();
}

void MultiFrame::loadXmlBak(){
  for (int i = 0; i < numFrameTag;i++){
    XML.pushTag("SCREENFRAME", i);
    screen_frames[i].setPos(XML.getValue("POS:X", 0, 0), XML.getValue("POS:Y", 0, 0));
    screen_frames[i].setRatio(XML.getValue("RATIO", 100, 0)/100.0);
    screen_frames[i].setAngle(XML.getValue("ANGLE", 0, 0));
    ofPoint warp_xml[4]; 
    cout << "num_warp " << XML.getNumTags("WARP") << endl; 
    int tt = 0;
    if (XML.getNumTags("WARPPT0") == 1){
      warp_xml[0].set(XML.getValue("WARPPT0:X",0,0),XML.getValue("WARPPT0:Y",0,0));
      tt++;
    }
    if (XML.getNumTags("WARPPT1") == 1){
      warp_xml[1].set(XML.getValue("WARPPT1:X",0,0),XML.getValue("WARPPT1:Y",0,0));
      tt++;
    }
    if (XML.getNumTags("WARPPT2") == 1){
      warp_xml[2].set(XML.getValue("WARPPT2:X",0,0),XML.getValue("WARPPT2:Y",0,0));
      tt++;
    }
    if (XML.getNumTags("WARPPT3") == 1){
      warp_xml[3].set(XML.getValue("WARPPT3:X",0,0),XML.getValue("WARPPT3:Y",0,0));
      tt++;
    }
    // for (int j = 0; j<4; j++){
    // 	cout << "wARP val " << XML.getValue("WARP:Y",0,j) << endl;
    // 	warp_xml[i].set(XML.getValue("WARP:X",0,j),XML.getValue("WARP:Y",0,j)) ;
    // }
    if (tt == 4){
      screen_frames[i].setWarpOut(warp_xml);
    }
    XML.popTag();
  }
}

void MultiFrame::saveToXmlBak(){
  XML.setValue("NUMSCREENFRAMES", n_screen_frames, 0);
  for (int i =0; i<n_screen_frames; i++){
    if  (i >= numFrameTag){
      XML.addTag("SCREENFRAME");
      numFrameTag++;
    }
    XML.pushTag("SCREENFRAME", i);
    ofPoint pos = screen_frames[i].getPos();
    XML.setValue("POS:X", pos.x, 0);
    XML.setValue("POS:Y", pos.y, 0);
    XML.setValue("RATIO", screen_frames[i].getRatio()*100);
    XML.setValue("ANGLE", screen_frames[i].getAngle());
    ofPoint warp_xml_out[4];
    for (int j = 0; j<4; j++){
      warp_xml_out[j] = screen_frames[i].getWarpPoint(j);
    }
    XML.setValue("WARPPT0:X", warp_xml_out[0].x, 0);
    XML.setValue("WARPPT0:Y", warp_xml_out[0].y, 0);

    XML.setValue("WARPPT1:X", warp_xml_out[1].x, 0);
    XML.setValue("WARPPT1:Y", warp_xml_out[1].y, 0);

    XML.setValue("WARPPT2:X", warp_xml_out[2].x, 0);
    XML.setValue("WARPPT2:Y", warp_xml_out[2].y, 0);

    XML.setValue("WARPPT3:X", warp_xml_out[3].x, 0);
    XML.setValue("WARPPT3:Y", warp_xml_out[3].y, 0);

    XML.popTag();
  }
  XML.saveFile("multiFrameBak.xml");
}

void MultiFrame::setImage(int i, ofxCvGrayscaleImage & image){
  screen_frames[i].setImage(image, OF_BLENDMODE_SCREEN);
  is_ok[i] = true;
  //screen_frames[i].setBlendMode(OF_BLENDMODE_SCREEN);
  n_screen_frames = max(n_screen_frames, i);
}

void MultiFrame::updateImage(int i, ofxCvGrayscaleImage & image){
  screen_frames[i].update(image);
}

void MultiFrame::setGrid(int w, int h, int n_col, int n_row){

  ofPoint cur_pos;
  int x_offset = anchorPercent.x * w;
  int y_offset = anchorPercent.y * h;

  int cur_row = 0;
  int cur_col = 0;

  for (int i = 0; i< n_screen_frames; i++){ 
    cur_pos.set(x_offset + cur_col * w, y_offset + cur_row * h);
    screen_frames[i].setPos(cur_pos);
    screen_frames[i].setSize(w, h);
    screen_frames[i].initWarp();
    screen_frames[i].setAngle(0);
    cur_col ++;
    if (cur_col >= n_col){
      cur_col = 0;
      cur_row ++;
    }
  }
}

void MultiFrame::setPos(int i, int x, int y){
  screen_frames[i].setPos(x, y);
}

void MultiFrame::setNumFrames(int n){
  n_screen_frames = n;
} 

int MultiFrame::getNumFrames(){
  return n_screen_frames;
} 

void MultiFrame::draw(bool draw_images, bool draw_frames){
  
  for (int i = 0; i < n_screen_frames; i++){
    if (is_ok[i]){
      if (draw_images){
	ofEnableBlendMode(OF_BLENDMODE_SCREEN); 
	screen_frames[i].drawImage();
	ofDisableBlendMode();
      }
      if (draw_frames){
	screen_frames[i].drawFrame();
      }
    }
  }
}


//--------------------------------------------------------------
void MultiFrame::mouseMoved(int x, int y) {
  for (int i = 0; i < n_screen_frames; i++){
    if (is_ok[i]){
      screen_frames[i].mouseMoved(x, y);
    }
  }
}

//--------------------------------------------------------------
void MultiFrame::mouseDragged(int x, int y, int button){
  for (int i = 0; i < n_screen_frames; i++){
    if (is_ok[i]){
      screen_frames[i].mouseDragged(x, y, button);
    }
  }
}

//--------------------------------------------------------------
void MultiFrame::mousePressed(int x, int y, int button){
  for (int i = 0; i < n_screen_frames; i++){
    if (is_ok[i]){
      screen_frames[i].mousePressed(x, y, button);
    }
  }
}

//--------------------------------------------------------------
void MultiFrame::mouseReleased(int x, int y, int button){
  for (int i = 0; i < n_screen_frames; i++){
    if (is_ok[i]){
      screen_frames[i].mouseReleased(x, y, button);
    }
  }
}


//--------------------------------------------------------------
MultiFrame::~MultiFrame()
{
    //dtor
}
