#ifndef LOOPRECORDERNUM_H
#define LOOPRECORDERNUM_H

#include "ofMain.h"


class LoopRecorderNum
{
    public:
        LoopRecorderNum();
        virtual ~LoopRecorderNum();

        void    setBufferLenght(int buffer_lenght_in);
        void    setLoop(int i_frame);
        void    setLoopStart(int i_start);
        void    setLoopStop(int i_stop);
        void    idle();

        int     getRecordMode();

        int     getCurrentFrameNum();
        int     getFrameStart();
        int     getFrameStop();

        void    play();
        void    stop();
        void    setPlayMode();

        int     getPlayMode();
        int     buffer_lenght;

    protected:
    private:

        int i_current_frame;

        int i_frame_start;
        int i_frame_stop;

        int loop_lenght;

        bool record_mode;
        bool play_mode;
};

#endif // LOOPRECORDERNUM_H
