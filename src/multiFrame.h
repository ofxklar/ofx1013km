#ifndef MULTIFRAME_H
#define MULTIFRAME_H

#define N_SCREEN_FRAME_MAX 64;

#include "ofxXmlSettings.h"
#include "utils.h"
#include "screenFrame.h"

class MultiFrame
{
 public:
  MultiFrame();
  virtual ~MultiFrame();

  void mouseMoved(int x, int y );
  void mouseDragged(int x, int y, int button);
  void mousePressed(int x, int y, int button);
  void mouseReleased(int x, int y, int button);

  void init(int n_frames);
  void loadXmlBak();
  void saveToXmlBak();
  void setGrid(int w, int h, int n_col, int n_row);
  void setImage(int i, ofxCvGrayscaleImage & image);
  void setPos(int i, int x, int y);
  void setNumFrames(int n);
  int getNumFrames();

  void updateImage(int i, ofxCvGrayscaleImage & image);

  void draw(bool draw_images, bool draw_frames);

  ofxXmlSettings XML;

 protected:
  
 private:
  
  int n_screen_frames;
  ScreenFrame screen_frames[64];
  bool is_ok[64];
  int numFrameTag;
  ofPoint anchorPercent;
};

#endif // SCREENFRAME_H
