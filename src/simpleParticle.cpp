#include "simpleParticle.h"

SimpleParticle::SimpleParticle(ofPoint position, ofPoint vitesse, int size_in){
  pos = position;
  vit = vitesse;

  size = size_in;
  color.set(255,255,255);

  lifespan = 4000;
  bAlive = true;

  birthdate = ofGetElapsedTimeMillis();
  old_date = ofGetElapsedTimeMillis();
  age = 0;
}

void SimpleParticle::update(){
  date = ofGetElapsedTimeMillis();
  step_duration = date - old_date;
  old_date = ofGetElapsedTimeMillis();

  age = date - birthdate;
  pos += vit * step_duration / 1000.0;
  
  bAlive = isAlive();
}

void SimpleParticle::draw(){
  ofSetColor(color);
  ofFill();
  cout << " pos.x " << pos.x <<" pos.y " << pos.y << endl; 
  ofCircle(pos.x, pos.y, size);
  ofSetColor(255,255,255);
  ofNoFill();
ofCircle(pos.x, pos.y, size);
}



bool SimpleParticle::isAlive(){
  if (age < lifespan){
    return true;
  }else{
    cout << "age " << age << " lifespan " << lifespan << endl;
    return false;
  }
}

ofPoint SimpleParticle::getPosition(){
  return pos;
}

ofPoint SimpleParticle::getVit(){
  return vit;
}

void SimpleParticle::setColor(int r, int g, int b){
  color.set(r, g, b);
}


SimpleParticle::~SimpleParticle(){
  
}
