# add custom variables to this file

# test suivi des modifications avec git
# OF_ROOT allows to move projects outside apps/* just set this variable to the
# absoulte --changer un mot a l'interieur -- path to the OF root folder

OF_ROOT = ../../..


# USER_CFLAGS allows to pass custom flags to the compiler
# for example search paths like:
# USER_CFLAGS = -I src/objects

USER_CFLAGS = -I /usr/include/libusb-1.0/ -DPD -DHAVE_UNISTD_H -DUSEAPI_DUMMY -shared 



# USER_LDFLAGS allows to pass custom flags to the linker
# for example libraries like:
# USER_LD_FLAGS = libs/libawesomelib.a

USER_LDFLAGS = #--export-dynamic



# use this to add system libraries for example:
# USER_LIBS = -lpango
 
USER_LIBS = -lusb-1.0 -ljack -ldl -lm -lportaudio


# change this to add different compiler optimizations to your project

USER_COMPILER_OPTIMIZATION = -march=native -mtune=native -Os


EXCLUDE_FROM_SOURCE="bin,.xcodeproj,obj"
