#ifndef K2MNOTE_H
#define K2MNOTE_H

#include "ofMain.h"
#include "ofxMidi.h"

class K2mNote
{
 public:
  K2mNote();
  virtual ~K2mNote();
  
  void init(ofxMidiOut & midi_out_in, int channel_in, int note_in, ofRectangle key_in);
  void update(int x, int y, int velocity_in);
  bool isInKey(int x, int y);
  void sendNoteOn(int velocity_in);
  void sendNoteOff();
  void draw();
  void unLock();

 protected:
 private:
  ofxMidiOut midi_out;
  bool lock;
  int channel;
  int note;
  ofRectangle key;
  bool send;
  int velocity;
  
};

#endif //NAKEDSTAIRS_H
