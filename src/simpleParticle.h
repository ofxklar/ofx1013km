#ifndef SIMPLE_PARTICLE_H
#define SIMPLE_PARTICLE_H

#include "utils.h"

class SimpleParticle
{
 public:
  SimpleParticle(ofPoint pos, ofPoint vit, int size);
  virtual ~SimpleParticle();
  
  void update();
  void draw();
  
  ofPoint getPosition();
  ofPoint getVit();

  void setColor(int r, int g, int b);
  bool isAlive();

 protected:
  ofPoint pos;
  ofPoint vit;
  int size;
  ofColor color;
  int birthdate;
  int age;
  int lifespan;
  bool bAlive;

  int date;
  int old_date;
  int step_duration;
};

#endif
