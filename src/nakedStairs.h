#ifndef NAKEDSTAIRS_H
#define NAKEDSTAIRS_H

//#include "testApp.h"
#include "utils.h"
#include "ofxOpenCv.h"
#include "LoopRecorder.h"

class NakedStairs
{
 public:
  NakedStairs();
  virtual ~NakedStairs();
  
  void init(int ns_vids_in, int lenght_in, int width, int height);
  void update();
  void draw(int x, int y, int w, int h, LoopRecorder & recorder);
  
  void setPosX(int pos_x_in);
  void setTransition(int i, int pos);
  void setNsnum(int n);
  void increaseNsnum();
  void decreaseNsnum();
  void setDelay(int n);
  void increaseDelay();
  void decreaseDelay();

 protected:
 private:
  
  int buffer_lenght;
  LoopRecorder recorder;
  int input_width;
  int input_height;

  int n_naked_stairs;
  int naked_stairs_delai;
  int naked_stairs_delai_255;
  int ns_old_delai;
  int ns_new_delai;
  int ns_lst_delai[64];
  int ns_delai;

  ofxCvGrayscaleImage   ns_gray[49];
  ofTexture             ns_rgba[49];
  int ns_transition[6];
  int ns_pos_x_init;
  int ns_size;
  int ns_width;
  int ns_height;
  int ns_nvids;
  int ns_num;


  // output
  int     ns_screen_width;
  int     ns_screen_height;
  int     ns_screen_position_x;
  int     ns_screen_position_y;
  float   ns_screen_ratio;
  bool    ns_draw_point_screen;
  bool    ns_corner_from_bak;
};

#endif //NAKEDSTAIRS_H
