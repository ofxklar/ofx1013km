#include "testApp.h"
#include "ofAppGlutWindow.h"

int main() {
	ofAppGlutWindow window;
	ofSetupOpenGL(&window, 2390, 800, OF_WINDOW);//OF_WINDOW);
	window.setWindowPosition(0,0);
	ofRunApp(new testApp());
}
